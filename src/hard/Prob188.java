package hard;

public class Prob188 {

    /**
     * https://www.youtube.com/watch?v=oDhu5uGq_ic&t=1349s
     *
     * @param k
     * @param prices
     * @return
     */
    public static int maxProfit(int k, int[] prices) {

        if (k == 0 || prices.length == 0) {
            return 0;
        }
        int T[][] = new int[k + 1][prices.length];

        for (int i = 1; i < T.length; i++) {
            for (int j = 1; j < T[0].length; j++) {
                int maxVal = 0;
                for (int m = 0; m < j; m++) {
                    maxVal = Math.max(maxVal, prices[j] - prices[m] + T[i - 1][m]);
                }
                T[i][j] = Math.max(T[i][j - 1], maxVal);
            }
        }
        return T[k][prices.length - 1];
    }

    public static void main(String[] args) {
        int[] arr = {3, 2, 6, 5, 0, 3};
        int k = 2;

        System.out.println(maxProfit(k, arr));
    }
}
