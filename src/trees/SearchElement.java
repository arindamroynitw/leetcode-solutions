package trees;


public class SearchElement {

    static boolean searchElement(Node root, int value) {
        if (root == null) {
            return false;
        }

        if (root != null && root.data == value) {
            return true;
        }
        return searchElement(root.left, value) || searchElement(root.right, value);
    }


    public static void main(String[] args) {
        Node root = Node.getStandardTree();
        System.out.println(searchElement(root, 5));
        System.out.println(searchElement(root, 8));
        System.out.println(searchElement(root, 1));
    }
}
