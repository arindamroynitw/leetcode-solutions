package trees;


public class InsertElement {

    static void insertElement(Node root, int value) {

        if (root == null) {
            root = new Node(value);
        } else {
            insertHelper(root, value);
        }
    }

    private static void insertHelper(Node root, int value) {
        if (root.left == null) {
            root.left = new Node(value);
        } else {
            insertElement(root.right, value);
        }
    }

    public static void main(String[] args) {
        Node root = Node.getStandardTree();

        TreeTraversals.preOrder(root);

        insertElement(root, 20);

        TreeTraversals.preOrder(root);

    }
}
