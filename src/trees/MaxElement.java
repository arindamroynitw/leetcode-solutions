package trees;

import static java.lang.Math.max;

public class MaxElement {

    static int maxElement(Node root) {
        //int max = Integer.MIN_VALUE;
        if (root != null) {

            int leftMax = maxElement(root.left);
            int rightMax = maxElement(root.right);

            return max((max(leftMax, rightMax)), root.data);

        }

        return -1;
    }

    public static void main(String[] args) {

        Node root = Node.getStandardTree();

        System.out.println(maxElement(root));


    }
}