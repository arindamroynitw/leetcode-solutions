package trees;

public class DeleteTree {

    //In Java, garbage collector does the job for you
    static void delete(Node root) {
        root = null;
    }

    static void delete(Node root, int value) {

        Node del, deepest = root;

        del = search(root, value);

        while (deepest.left != null) {
            deepest = deepest.left;
        }


        del.data = deepest.data;
        deepest =null;

    }

    private static Node search(Node root, int value) {
        if (root == null) {
            return null;
        }
        if (root.data == value) {
            return root;
        } else {
            if (search(root.left, value) != null) {
                return search(root.left, value);
            } else {
                return search(root.right, value);
            }
        }
    }

    public static void main(String[] args) {
        Node root = Node.getStandardTree();

        TreeTraversals.preOrder(root);
        delete(root, 5);
        TreeTraversals.preOrder(root);
    }
}
