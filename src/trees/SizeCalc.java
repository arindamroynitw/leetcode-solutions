package trees;

public class SizeCalc {

    static int size(Node root) {

        if (root == null) {
            return 0;
        }

        return 1 + size(root.left) + size(root.right);
    }



    public static void main(String[] args) {
        Node root = Node.getStandardTree();

        Node root1 = new Node(5);
        root1.left = new Node(100);
        root1.left.left = new Node(55);
        root1.left.left.right = new Node(4);
        System.out.println(size(root1));
    }
}
