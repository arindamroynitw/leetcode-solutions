package graphs;

import java.util.ArrayDeque;
import java.util.Deque;

public class BFS {

    static void bfs(Graph graph, int node) {

        boolean[] visited = new boolean[graph.vertices];

        Deque<Integer> queue = new ArrayDeque<>();

        visited[node] = true;
        queue.add(node);

        while (!queue.isEmpty()) {
            int curr = queue.remove();
            System.out.print(curr + " - ");

            for (Integer x : graph.adjacencyListArray[curr]) {
                if (!visited[x]) {
                    visited[x] = true;
                    queue.add(x);
                }
            }
        }

    }

    public static void main(String[] args) {
        Graph g = Graph.getStandardGraph();

        bfs(g, 0);
    }
}
