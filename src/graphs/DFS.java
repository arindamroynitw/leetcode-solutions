package graphs;

public class DFS {

    static void dfshelper(Graph graph, int node, boolean[] visited) {

        visited[node] = true;

        System.out.print(node + " - ");

        for (Integer x : graph.adjacencyListArray[node]) {
            if (!visited[x]) {
                dfshelper(graph, x, visited);
            }
        }

    }

    static void dfs(Graph graph, int vertex) {

        boolean[] visited = new boolean[graph.vertices];
        dfshelper(graph, vertex, visited);
    }

    public static void main(String[] args) {

        Graph graph = new Graph(6);
        graph.addEdge(5, 0);
        graph.addEdge(5, 2);
        graph.addEdge(2, 3);
        graph.addEdge(3, 1);
        graph.addEdge(4, 1);
        graph.addEdge(4, 0);
        dfs(graph, 5);

    }
}
