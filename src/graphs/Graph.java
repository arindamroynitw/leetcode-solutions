package graphs;

import java.util.LinkedList;

public class Graph {

    int vertices;

    LinkedList<Integer>[] adjacencyListArray;

    public Graph(int v) {
        vertices = v;
        adjacencyListArray = new LinkedList[v];

        for (int i = 0; i < adjacencyListArray.length; i++) {
            adjacencyListArray[i] = new LinkedList<>();
        }
    }

    void addEdge(int src, int dest) {
        this.adjacencyListArray[src].add(dest);
    }

    void addEdgeBidirectional(int src, int dest) {
        this.adjacencyListArray[src].add(dest);
        this.adjacencyListArray[dest].add(src);
    }

    void removeEdge(int src, int dest) {
        this.adjacencyListArray[src].remove(dest);
    }

    static Graph getStandardGraph() {
        Graph g = new Graph(4);
        g.addEdge(0, 1);
        g.addEdge(0, 2);
        g.addEdge(1, 2);
        g.addEdge(2, 0);
        g.addEdge(2, 3);
        g.addEdge(3, 3);
        return g;
    }
}
