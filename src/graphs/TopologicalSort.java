package graphs;

import java.util.Stack;

public class TopologicalSort {

    static void topologicalSort(Graph graph) {

        Stack<Integer> result = new Stack<>();

        boolean[] visited = new boolean[graph.vertices];

        for (int i = 0; i < graph.vertices; i++) {
            tsUtil(graph, i, result, visited);
        }

        while (!result.isEmpty()) {
            System.out.print(result.pop() + " - ");
        }


    }

    private static void tsUtil(Graph graph, int node, Stack<Integer> result, boolean[] visited) {

        if (visited[node]) {
            return;
        }

        visited[node] = true;
        for (Integer x : graph.adjacencyListArray[node]) {
            if (!visited[x]) {
                tsUtil(graph, x, result, visited);
            }
        }
        result.push(node);
    }

    public static void main(String[] args) {

        Graph graph = new Graph(6);
        graph.addEdge(5, 0);
        graph.addEdge(5, 2);
        graph.addEdge(2, 3);
        graph.addEdge(3, 1);
        graph.addEdge(4, 1);
        graph.addEdge(4, 0);
        topologicalSort(graph);
    }
}
