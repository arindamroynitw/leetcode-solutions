package medium;

/**
 * Given n non-negative integers a1, a2, ..., an ,
 * where each represents a point at coordinate (i, ai).
 * n vertical lines are drawn such that the two endpoints of line i is at (i, ai) and (i, 0).
 * Find two lines, which together with x-axis forms a container, such that the container contains the most water.
 * <p>
 * Note: You may not slant the container and n is at least 2.
 * <p>
 * Ref video - https://youtu.be/TI3e-17YAlc
 */
public class Prob11 {

    public int maxArea(int[] height) {

        int result = Integer.MIN_VALUE;

        int i = 0;

        int j = height.length - 1;

        while (i < j) {
            int min = Math.min(height[i], height[j]);
            result = Math.max(result, min * (j - i));

            if (min == height[i]) {
                i++;
            } else {
                j--;
            }
        }
        return result;
    }
}
