package medium;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Given an array nums of n integers, are there elements a, b, c in nums such that a + b + c = 0?
 * Find all unique triplets in the array which gives the sum of zero.
 */
public class Prob15 {

    public List<List<Integer>> threeSum(int[] nums) {

        Set<List<Integer>> result = new HashSet<>();

        for (int i = 0; i < nums.length; i++) {
            int target = -nums[i];
            Map<Integer, Integer> map = new HashMap<>();

            for (int j = 0; j < nums.length && j != i; j++) {
                if (map.containsKey(target - nums[j])) {
                    result.add(Arrays.asList(nums[i], nums[j], target - nums[j]));
                } else {
                    map.put(nums[j], j);
                }
            }

        }

        return result.stream().collect(Collectors.toList());
    }


    public List<List<Integer>> _3sum(int[] nums) {

        if(nums.length==0) {
            return new ArrayList<>();
        }

        Set<List<Integer>> result = new HashSet<>();

        Arrays.sort(nums);

        for (int i = 0; i < nums.length; i++) {
            int target = -nums[i];

            int x = i + 1, y = nums.length - 1;

            while (x < y) {
                if (nums[x] + nums[y] == target) {
                    result.add(Arrays.asList(nums[i], nums[x], nums[y]));
                    x++;
                    y--;
                } else if (nums[x] + nums[y] > target) {
                    y--;
                } else {
                    x++;
                }

            }
        }

        return result.stream().collect(Collectors.toList());
    }

    public static void main(String[] args) {
        System.out.println(new Prob15().threeSum(new int[]{-1, 0, 1, 2, -1, -4}));
        System.out.println(new Prob15()._3sum(new int[]{-1, 0, 1, 2, -1, -4}));

    }
}
