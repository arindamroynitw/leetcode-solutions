package medium;

public class Prob5 {
    public String longestPalindrome(String s) {

        if (s == null || s.length() == 0) {
            return "";
        }
        int n = s.length();

        int maxLength = 1;
        int start = 0;

        //if isPlaindrome[i][j] == true, String from i to j is palindrome
        boolean[][] isPalindrome = new boolean[n][n];


        //Step 1 - All single letters are palindromic

        for (int i = 0; i < n; i++) {
            isPalindrome[i][i] = true;
        }

        //Step 2 - All two letter words are palindromes if
        //same characater.
        for (int i = 0; i < n - 1; i++) {
            if (s.charAt(i) == s.charAt(i + 1)) {
                isPalindrome[i][i + 1] = true;
                maxLength = 2;
                start = i;
            } else {
                isPalindrome[i][i + 1] = false;
            }

        }

        //Step 3 - find all palindromes of length three or more

        //Fixed pal size
        for (int palSize = 3; palSize <= n; palSize++) {
            for (int i = 0; i < n - palSize + 1; i++) {

                int j = i + palSize - 1;

                if (isPalindrome[i + 1][j - 1] && s.charAt(i) == s.charAt(j)) {
                    isPalindrome[i][j] = true;

                    if (palSize > maxLength) {
                        maxLength = palSize;
                        start = i;
                    }
                }
            }
        }

        return s.substring(start, start + maxLength);
    }

    public static void main(String[] args) {
        Prob5 prob5 = new Prob5();

        System.out.println(prob5.longestPalindrome("forgeeksskeegfor"));
    }

}
