package medium;

public class Prob8 {

    public int myAtoi(String str) {

        if (str == null || str.length() == 0) {
            return 0;
        }

        char[] inp = str.toCharArray();

        long result = 0;

        boolean neg = false;

        int i;
        for (i = 0; i < inp.length; i++) {

            if (inp[i] > '9' || inp[i] < '0') {
                if (inp[i] == ' ') {
                    continue;
                }
                if (inp[i] == '-') {
                    neg = true;
                    break;
                } else if (inp[i] == '+') {
                    break;
                } else {
                    return 0;
                }
            } else {
                break;
            }
        }

        if (i == inp.length) {
            return 0;
        }

        int j = i;

        if (inp[i] == '-' || inp[i] == '+') {
            if (i != inp.length - 1) {
                j = i + 1;
            } else {
                return 0;
            }

        }

        if (inp[j] > '9' || inp[j] < '0') {
            return 0;
        }

        for (; j < inp.length; j++) {

            if (inp[j] >= '0' && inp[j] <= '9') {
                long temp = result * 10 + (inp[j] - '0');

                if (temp > Integer.MAX_VALUE || temp < Integer.MIN_VALUE) {
                    return neg ? Integer.MIN_VALUE : Integer.MAX_VALUE;
                } else {
                    result = temp;
                }
            } else {
                break;
            }
        }

        return neg ? (int) (0 - result) : (int) result;

    }

    public static void main(String[] args) {

        Prob8 prob8 = new Prob8();

        System.out.println(prob8.myAtoi("2147483648"));
    }


}
