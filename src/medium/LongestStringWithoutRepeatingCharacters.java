package medium;

import java.util.HashMap;
import java.util.Map;

/**
 * Given a string, find the length of the longest substring without repeating characters.
 * <p>
 * Example 1:
 * <p>
 * Input: "abcabcbb"
 * Output: 3
 * Explanation: The answer is "abc", with the length of 3.
 * Example 2:
 * <p>
 * Input: "bbbbb"
 * Output: 1
 * Explanation: The answer is "b", with the length of 1.
 * Example 3:
 * <p>
 * Input: "pwwkew"
 * Output: 3
 * Explanation: The answer is "wke", with the length of 3.
 * Note that the answer must be a substring, "pwke" is a subsequence and not a substring.
 */
public class LongestStringWithoutRepeatingCharacters {


    /**
     * We used recursion here. Basically, find the character that caused a match in the substring, and then
     * recurse over the substring from after that character.
     *
     * Eg - x(dvdf) = max(x(dvdf), x(vdf)) since d is the matched character.
     */
    public int lengthOfLongestSubstring(String s) {

        if (s.length() == 0) return 0;
        Map<Character, Integer> myMap = new HashMap<>();

        int result = 1;
        int curr = 0;

        for (int i = 0; i < s.length(); i++) {
            if (myMap.containsKey(s.charAt(i))) {
                return Math.max(curr, lengthOfLongestSubstring(s.substring(myMap.get(s.charAt(i)) + 1)));
            } else {
                myMap.put(s.charAt(i), i);
                curr += 1;
            }
        }
        return Math.max(result, curr);
    }


    public static void main(String[] args) {

        LongestStringWithoutRepeatingCharacters l = new LongestStringWithoutRepeatingCharacters();
        System.out.println(l.lengthOfLongestSubstring("dvdf"));
        System.out.println(l.lengthOfLongestSubstring("au"));
        System.out.println(l.lengthOfLongestSubstring("abcabcbb"));
        System.out.println(l.lengthOfLongestSubstring("bbbbb"));
        System.out.println(l.lengthOfLongestSubstring("pwwkew"));
    }
}
