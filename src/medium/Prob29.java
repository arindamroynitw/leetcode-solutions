package medium;

import easy.Prob20;

/**
 * Given two integers dividend and divisor, divide two integers without using multiplication, division and mod operator.
 * <p>
 * Return the quotient after dividing dividend by divisor.
 * <p>
 * The integer division should truncate toward zero.
 * <p>
 * Example 1:
 * <p>
 * Input: dividend = 10, divisor = 3
 * Output: 3
 * Example 2:
 * <p>
 * Input: dividend = 7, divisor = -3
 * Output: -2
 */
public class Prob29 {

    public int divide(int dividend, int divisor) {

        int quotient = 0;

        boolean resultSign = (dividend < 0) == (divisor < 0);

        int ndividend = Math.abs(dividend);
        int ndivisor = Math.abs(divisor);


        while (ndividend >= ndivisor) {
            int temp = ndividend - ndivisor;
            if ((temp + ndivisor) != ndividend) {
                return Integer.MAX_VALUE;
            }
            ndividend = temp;
            quotient++;
        }

        return resultSign ? quotient : -quotient;

    }

    public static void main(String[] args) {

        Prob29 prob29 = new Prob29();

        System.out.println(prob29.divide(-2147483648
                , -1));

    }
}
