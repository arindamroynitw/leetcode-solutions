package medium;

class DeleteNthFromEnd {
    public ListNode removeNthFromEnd(ListNode head, int n) {

        ListNode slow = head;
        ListNode fast = head;

        for (int i = 0; i < n; i++) {
            fast = fast.next;
        }

        if (fast == null) {
            return head.next;
        }

        while (fast != null && fast.next != null) {
            fast = fast.next;
            slow = slow.next;
        }


        slow.next = slow.next.next;

        return head;

    }

    void display(ListNode head) {
        while (head != null) {
            System.out.print(head.val + "->");
            head = head.next;
        }
        System.out.println();
    }
}

public class Prob19 {

    public static void main(String[] args) {
        DeleteNthFromEnd s = new DeleteNthFromEnd();

        ListNode head = new ListNode(1);
        //head.next = new ListNode(2);
//        head.next.next = new ListNode(3);
//        head.next.next.next = new ListNode(4);
//        head.next.next.next.next = new ListNode(5);

        s.display(head);

        ListNode result = s.removeNthFromEnd(head, 1);

        s.display(result);
    }
}
