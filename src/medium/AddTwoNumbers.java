package medium;

/**
 * You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse order and each of their nodes contain a single digit. Add the two numbers and return it as a linked list.
 * <p>
 * You may assume the two numbers do not contain any leading zero, except the number 0 itself.
 * <p>
 * Example:
 * <p>
 * Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
 * Output: 7 -> 0 -> 8
 * Explanation: 342 + 465 = 807.
 */
public class AddTwoNumbers {

    public ListNode addTwoNumbersPerfect(ListNode l1, ListNode l2) {
        ListNode dummyHead = new ListNode(0);
        ListNode p = l1, q = l2, curr = dummyHead;
        int carry = 0;
        while (p != null || q != null) {
            int x = (p != null) ? p.val : 0;
            int y = (q != null) ? q.val : 0;
            int sum = carry + x + y;
            carry = sum / 10;
            curr.next = new ListNode(sum % 10);
            curr = curr.next;
            if (p != null) p = p.next;
            if (q != null) q = q.next;
        }
        if (carry > 0) {
            curr.next = new ListNode(carry);
        }
        return dummyHead.next;
    }

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {

        ListNode result = new ListNode(-1);
        ListNode resultPtr = result;
        int carryOver = 0   ;
        while (l1 != null && l2 != null) {
            int temp = l1.val + l2.val + carryOver;

            if (temp < 10) {
                result.val = temp;
                carryOver = 0;
            } else {
                result.val = temp - 10;
                carryOver = 1;
            }
            l1 = l1.next;
            l2 = l2.next;
            result.next = new ListNode(-1);
            result = result.next;
        }

        if (l1 != null) {
            while (l1 != null) {

                int temp = l1.val + carryOver;
                if (temp < 10) {
                    result.val = temp;
                    carryOver = 0;
                } else {
                    result.val = temp - 10;
                    carryOver = 1;
                }
                l1 = l1.next;
                result.next = new ListNode(-1);
                result = result.next;
            }
        }

        if (l2 != null) {
            while (l2 != null) {
                int temp = l2.val + carryOver;
                if (temp < 10) {
                    result.val = temp;
                    carryOver = 0;
                } else {
                    result.val = temp - 10;
                    carryOver = 1;
                }
                l2 = l2.next;
                result.next = new ListNode(-1);
                result = result.next;
            }
        }

        result = resultPtr;

        while (result.next != null) {
            if (result.next.val == -1) {
                result.next = null;
                break;
            }
            result = result.next;
        }

        if (carryOver == 1) {
            result.next = new ListNode(1);
        }

        return resultPtr;
    }

    public static void main(String[] args) {
        AddTwoNumbers addTwoNumbers = new AddTwoNumbers();
        ListNode l1 = new ListNode(3);
        l1.next = new ListNode(9);
        l1.next.next = new ListNode(9);

        ListNode l2 = new ListNode(9);
        l2.next = new ListNode(1);
//        l2.next.next = new ListNode(3);

        System.out.println(addTwoNumbers.addTwoNumbers(l1, l2));
    }

}


