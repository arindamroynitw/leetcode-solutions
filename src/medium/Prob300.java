package medium;

public class Prob300 {

    public static int lengthOfLIS(int[] nums) {

        return lengthHelper(nums, nums.length);
    }

    private static int lengthHelper(int[] nums, int length) {
        if (length == 1) {
            return 1;
        }
        if (nums[length - 1] > nums[length - 2]) {
            return 1 + lengthHelper(nums, length - 1);
        } else {
            return lengthHelper(nums, length - 1);
        }
    }

    public static void main(String[] args) {
        System.out.println(lengthOfLIS(new int[]{3, 4, -1, 0, 6, 2, 3}));
    }

}
