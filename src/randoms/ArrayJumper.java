package randoms;

import java.util.Arrays;

/**
 * Jumps needed to escape the array (last element reached).
 * <p>
 * Video ref - https://www.youtube.com/watch?v=cETfFsSTGJI
 */
public class ArrayJumper {

    //minJumps(start, end) = Min ( minJumps(k, end) ) for all k reachable from start
    //Recursive sol
    public static int minmimumJumps(int[] heights) {
        return helper(heights, 0, heights.length - 1);
    }

    private static int helper(int[] heights, int start, int end) {

        if (heights[start] == 0) {
            return Integer.MAX_VALUE;
        }

        if (start == end) {
            return 0;
        }


        int min = Integer.MAX_VALUE;
        for (int i = start + 1; i <= end && i <= start + heights[start]; i++) {
            int jumps = helper(heights, i, end);

            if (jumps != Integer.MAX_VALUE && jumps + 1 < min) {
                min = jumps + 1;
            }
        }

        return min;


    }

    //Dynamic sol
    public static int minimumNoOfJumps(int[] heights) {

        int[] jumps = new int[heights.length];
        int[] prev = new int[heights.length];

        Arrays.fill(jumps, Integer.MAX_VALUE);

        int i = 1, j = 0;

        jumps[0] = 0;

        for (i = 1; i < heights.length; i++) {
            for (j = 0; j < i; j++) {
                if ((j + heights[j]) >= i) {
                    int curr = 1 + jumps[j];
                    if (curr < jumps[i]) {
                        jumps[i] = curr;
                        prev[i] = j;
                    }
                }
            }
        }

        return jumps[jumps.length - 1];

    }

    public static void main(String[] args) {
        System.out.println(minimumNoOfJumps(new int[]{2, 3, 1, 1, 2, 4, 2, 0, 1}));
        System.out.println(minmimumJumps(new int[]{2, 3, 1, 1, 2, 4, 2, 0, 1}));
    }

}
