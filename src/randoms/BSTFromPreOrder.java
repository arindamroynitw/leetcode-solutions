package randoms;

/**
 * 1. Create an empty stack.
 * <p>
 * 2. Make the first value as root. Push it to the stack.
 * <p>
 * 3. Keep on popping while the stack is not empty and the next value is greater than stack’s top value.
 * Make this value as the right child of the last popped node. Push the new node to the stack.
 * <p>
 * 4. If the next value is less than the stack’s top value, make this value as the left child of the stack’s top node. Push the new node to the stack.
 * <p>
 * 5. Repeat steps 2 and 3 until there are items remaining in pre[].
 */
class Node {
    int data;

    Node left, right;

    public Node(int data) {
        this.data = data;
        left = right = null;
    }
}

public class BSTFromPreOrder {

    static Node buildTree(int[] inOrder, int[] postOrder) {

        int n = inOrder.length;

        Index pIndex = new Index();
        pIndex.index = n - 1;

        return buildUtil(inOrder, postOrder, 0, n - 1, pIndex);
    }

    private static Node buildUtil(int[] inOrder, int[] postOrder, int start, int end, Index pIndex) {

        if (start > end) {
            return null;
        }


        Node node = new Node(postOrder[pIndex.index]);
        (pIndex.index)--;
        if (start == end) {
            return node;
        }

        int iIndex = search(inOrder, start, end, node.data);

        node.right = buildUtil(inOrder, postOrder, iIndex + 1, end, pIndex);
        node.left = buildUtil(inOrder, postOrder, start, iIndex - 1, pIndex);

        return node;

    }

    private static int search(int[] inOrder, int start, int end, int data) {
        int i = 0;
        for (; i <= end; i++) {
            if (inOrder[i] == data) {
                break;
            }
        }
        return i;
    }


    public static void main(String[] args) {
        int in[] = new int[]{4, 8, 2, 5, 1, 6, 3, 7};
        int post[] = new int[]{8, 4, 5, 2, 6, 7, 3, 1};

        Node root = buildTree(in, post);
    }

}

class Index {
    int index;
}



