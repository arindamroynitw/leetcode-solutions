package randoms;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;

public class SlidingWindowMaximum {

    //Given an array A and an integer K. Find the maximum for each and every contiguous subarray of size K.

    // A Dequeue (Double ended queue) based method for printing maixmum element of
    // all subarrays of size k
    static void printMax(int arr[], int k) {

        Deque<Integer> queue = new ArrayDeque<>();

        int i = 0;

        for (; i < k; i++) {

            while (!queue.isEmpty() && queue.peekLast() <= arr[i]) {
                queue.removeLast();
            }

            if (queue.isEmpty() || queue.peekLast() > arr[i]) {
                queue.addLast(i);
            }
        }

        for (; i < arr.length; i++) {
            System.out.println(arr[queue.peekFirst()]);

            while (!queue.isEmpty() && queue.peekFirst() <= i - k) {
                queue.removeFirst();
            }

            while (!queue.isEmpty() && arr[queue.peekLast()] <= arr[i]) {
                queue.removeLast();
            }

            if (queue.isEmpty() || arr[queue.peekLast()] > arr[i]) {
                queue.addLast(i);
            }
        }

        if (!queue.isEmpty()) {
            System.out.println(arr[queue.peekFirst()]);
        }

    }


    public static void main(String[] args) {
        int arr[] = {7, 10, 20, 65, -18, 7, 4, 3};
        int k = 3;
        printMax(arr, k);
    }
}
