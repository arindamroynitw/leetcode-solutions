package randoms;

/**
 *
 * Note - subsequence means the letters need not be adjacent, but MUST BE IN THE SAME ORDER.
 * If they say order is not important, than this is trivial character matching problem.
 *
 * Fundamental is this -
 *
 * if str[i]!=str[j] then just see whatever was last best, i.e., best of taking [i-1][j] or [i][j-1]
 * Else, take current match to increase count of [i-1][j-1] by 1.
 *
 *
 * Video ref - https://youtu.be/NnD96abizww
 *
 * Text ref - https://www.geeksforgeeks.org/longest-common-subsequence-dp-4/
 */
public class LongestCommonSubsequence {


    public static int lcsRecursive(String input1, String input2) {

        char[] arr1 = input1.toCharArray();
        char[] arr2 = input2.toCharArray();

        return lcsHelper(arr1, arr2, arr1.length, arr2.length);

    }

    private static int lcsHelper(char[] arr1, char[] arr2, int l1, int l2) {
        if (l1 == 0 || l2 == 0) {
            return 0;
        }

        if (arr1[l1 - 1] == arr2[l2 - 1]) {
            return 1 + lcsHelper(arr1, arr2, l1 - 1, l2 - 1);
        } else {
            return Math.max(lcsHelper(arr1, arr2, l1 - 1, l2), lcsHelper(arr1, arr2, l1, l2 - 1));
        }
    }

    public static int lcsDynamic(String input1, String input2) {

        char[] arr1 = input1.toCharArray();
        char[] arr2 = input2.toCharArray();

        int[][] result = new int[arr1.length + 1][arr2.length + 1];

        for (int i = 0; i <= arr1.length; i++) {
            result[i][0] = 0;
        }

        for (int j = 0; j <= arr2.length; j++) {
            result[0][j] = 0;
        }

        int i = 0;
        int j = 0;
        int max = 0;
        for (i = 1; i <= arr1.length; i++) {
            for (j = 1; j <= arr2.length; j++) {
                if (arr1[i - 1] == arr2[j - 1]) {
                    result[i][j] = result[i - 1][j - 1] + 1;
                } else {
                    result[i][j] = Math.max(result[i - 1][j], result[i][j - 1]);
                }

                max = Math.max(result[i][j], max);
            }
        }

        return max;

    }


    public static void main(String[] args) {
        System.out.println(lcsDynamic("arindam", "aman"));
        System.out.println(lcsRecursive("ABCDGHA", "AEDFHRA"));
    }

}
