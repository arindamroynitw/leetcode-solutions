package randoms;

/**
 * Given an array of n elements which contains elements
 * from 0 to n-1, with any of these numbers appearing any number of times.
 * Find these repeating numbers in O(n) and using only constant memory space.
 * For example, let n be 7 and array be {1, 2, 3, 1, 3, 6, 6},
 * the answer should be 1, 3 and 6.
 */
public class RepeatingElementsInArray {

    public static int[] repeating(int[] list) {

        int counter = 0;
        int[] dummy = new int[list.length / 2];

        for (int i = 0; i < list.length; i++) {
            if (list[Math.abs(list[i])] >= 0) {
                list[Math.abs(list[i])] = -list[Math.abs(list[i])];
            } else {
                dummy[counter++] = Math.abs(list[i]);
            }
        }

        int[] result = new int[counter];
        for (int i = 0; i < counter; i++) {
            result[i] = dummy[i];
        }

        return result;
    }

    public static void main(String[] args) {

        int[] ans = repeating(new int[]{1, 2, 0, 1});

        for (int x : ans) {
            System.out.print(x + " ");
        }
    }

}
