package randoms;

public class Knapsack {

    static int knapSackDynamic(int[] weight, int[] value, int target) {

        int[][] result = new int[weight.length + 1][target + 1];

        for (int i = 0; i <= weight.length; i++) {
            for (int j = 0; j <= target; j++) {
                if (i == 0 || j == 0) {
                    result[i][j] = 0;
                    continue;
                }

                if (weight[i - 1] <= j) {
                    result[i][j] = Math.max(result[i - 1][j], result[i - 1][j - weight[i - 1]] + value[i - 1]);
                } else {
                    result[i][j] = result[i - 1][j];
                }
            }
        }

        return result[weight.length][target];

    }

    static int knapSack(int[] weight, int[] value, int target) {

        return helper(weight, value, target, weight.length);

    }

    private static int helper(int[] weight, int[] value, int target, int length) {
        if (length == 0 || target == 0) {
            return 0;
        }

        if (weight[length - 1] > target) {
            return helper(weight, value, target, length - 1);
        } else {
            return Math.max(helper(weight, value, target, length - 1), value[length - 1] + helper(weight, value, target - weight[length - 1], length - 1));
        }
    }

    public static void main(String[] args) {
        System.out.println(knapSack(new int[]{1, 3, 4, 5}, new int[]{1, 4, 5, 7}, 7));
        System.out.println(knapSackDynamic(new int[]{1, 3, 4, 5}, new int[]{1, 4, 5, 7}, 7));

    }
}
