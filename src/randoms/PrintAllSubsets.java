package randoms;

/**
 * Video ref - https://www.youtube.com/watch?v=bGC2fNALbNU
 */
public class PrintAllSubsets {

    void printSubsets(int[] arr) {

        int[] result = new int[arr.length];
        helper(arr, result, 0);
    }

    private void helper(int[] arr, int[] result, int i) {
        if (i == arr.length) {
            printArray(result);
            return;
        }

        //Choice 1 : Don't pick element at i
        result[i] = Integer.MIN_VALUE;
        helper(arr, result, i + 1);

        //Choice 2 : Pick element at i
        result[i] = arr[i];
        helper(arr, result, i + 1);

    }

    private void printArray(int[] result) {
        System.out.print("Set ==> ");
        for (int i = 0; i < result.length; i++) {
            if (result[i] != Integer.MIN_VALUE) {
                System.out.print(result[i] + " - ");
            }
        }
        System.out.println();
    }

    public static void main(String[] args) {
        PrintAllSubsets printAllSubsets = new PrintAllSubsets();
        printAllSubsets.printSubsets(new int[]{1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 20, 21});
    }
}
