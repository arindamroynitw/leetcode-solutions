package hashtable;

import java.util.ArrayList;
import java.util.List;

/**
 * @author arindamroy
 */

class HashEntry {

    private List<Integer> data;

    public HashEntry() {
        data = new ArrayList<>();
    }

    public List<Integer> getData() {
        return data;
    }

    public void setData(List<Integer> data) {
        this.data = data;
    }
}

class MyHashSet {

    HashEntry[] dataStore = new HashEntry[10];

    /**
     * Initialize your data structure here.
     */
    public MyHashSet() {

        for (int i = 0; i < 10; i++) {
            dataStore[i] = new HashEntry();
        }
    }

    public void add(int key) {
        HashEntry hashEntry = dataStore[hashFunction(key)];

        if (hashEntry.getData() == null || hashEntry.getData().size() == 0) {
            hashEntry.getData().add(key);
            return;
        }

        if (!contains(key)) {
            hashEntry.getData().add(key);
        }


    }

    public void remove(int key) {
        List<Integer> data = dataStore[hashFunction(key)].getData();

        if (data != null && data.size() > 0 && data.contains(key)) {
            boolean isremoved = data.remove((Object) key);
        }
    }

    /**
     * Returns true if this set contains the specified element
     */
    public boolean contains(int key) {
        return dataStore[hashFunction(key)].getData().contains(key);
    }


    private int hashFunction(int key) {
        return key % 10;
    }


    public static void main(String[] args) {
        //["MyHashSet","add","add","contains","contains","add","contains","remove","contains"]
        //[[],[1],[2],[1],[3],[2],[2],[2],[2]]

        MyHashSet myHashSet = new MyHashSet();
        myHashSet.add(1);
        myHashSet.add(2);
        myHashSet.contains(1);
        myHashSet.contains(3);
        myHashSet.add(2);
        myHashSet.contains(2);
        myHashSet.remove(2);
        myHashSet.contains(2);

    }
}

/**
 * Your MyHashSet object will be instantiated and called as such:
 * MyHashSet obj = new MyHashSet();
 * obj.add(key);
 * obj.remove(key);
 * boolean param_3 = obj.contains(key);
 */
