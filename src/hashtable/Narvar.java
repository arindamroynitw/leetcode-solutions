package hashtable;

import java.util.ArrayList;
import java.util.List;

/*
 * To execute Java, please define "static void main" on a class
 * named Solution.
 *
 * If you need more classes, simply define them inline.

 Implement map which maintains insertion order.

 It should also have all the properties a Map.
 */
class NarvarNode {
    public int key;
    public int value;
    public NarvarNode prev;
    public NarvarNode next;

    public NarvarNode(int key, int value) {
        this.key = key;
        this.value = value;
    }

}

class NarvarMapEntry {
    List<NarvarNode> data;

    public NarvarMapEntry() {
        data = new ArrayList<>();
    }
}

class Narvar {

    private NarvarMapEntry[] datasource = new NarvarMapEntry[10];

    NarvarNode firstEntry = null;
    NarvarNode lastEntry = null;

    public Narvar() {

        for (int i = 0; i < 10; i++) {
            datasource[i] = new NarvarMapEntry();
        }

    }


    public void put(int key, int value) {

        NarvarMapEntry curr = datasource[hash(key)];

        if (curr == null || curr.data.size() == 0 || !contains(key)) {
            NarvarNode newNode = new NarvarNode(key, value);
            curr.data.add(newNode);
            newNode.prev = lastEntry;

            if (lastEntry != null) {
                lastEntry.next = newNode;
            } else {
                firstEntry = newNode;
            }

            lastEntry = newNode;
            return;
        }

        for (NarvarNode data : curr.data) {
            if (data.key == key) {
                data.value = value;
            }
        }
    }

    public int get(int key) {

        NarvarMapEntry curr = datasource[hash(key)];

        if (curr == null || curr.data.size() == 0 || !contains(key)) {
            return -1;
        }

        for (NarvarNode data : curr.data) {
            if (data.key == key) {
                return data.value;
            }
        }

        return -1;
    }


    public boolean contains(int key) {
        NarvarMapEntry curr = datasource[hash(key)];

        if (curr == null || curr.data.size() == 0) {
            return false;
        }

        for (NarvarNode data : curr.data) {
            if (data.key == key) {
                return true;
            }
        }
        return false;

    }

    public void print() {
        if (firstEntry == null) {
            return;
        }

        NarvarNode temp = firstEntry;

        while (temp != null) {
            System.out.println(temp.key + "- > " + temp.value);
            temp = temp.next;
        }
    }

    private int hash(int key) {
        return key % 10;
    }

}

class NarvarSol {
    public static void main(String[] args) {

        Narvar map = new Narvar();

        map.put(4, 8);
        map.put(3, 6);
        map.put(2, 4);
        map.put(14, 99);
        map.put(1, 2);
        map.print();

    }


}
