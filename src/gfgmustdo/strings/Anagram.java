package gfgmustdo.strings;

import java.util.HashMap;
import java.util.Map;

/**
 * Write a function to check whether two given strings are anagram of each other or not.
 * An anagram of a string is another string that contains same characters, only the order of
 * characters can be different. For example, “abcd” and “dabc” are anagram of each other.
 */
public class Anagram {

    static boolean areAnagram(char[] str1, char[] str2) {

        Map<Character, Integer> map = new HashMap<>();

        for (char ch : str1) {
            if (map.containsKey(ch)) {
                int temp = map.get(ch);
                map.put(ch, temp + 1);
            } else {
                map.put(ch, 1);
            }
        }

        for (char ch : str2) {
            if (!map.containsKey(ch) || map.get(ch) == 0) {
                return false;
            } else {
                int temp = map.get(ch);
                map.put(ch, temp - 1);
            }
        }

        return true;
    }

    public static void main(String[] args) {

        String s1 = "ilovecode";
        String s2 = "cedelovei";
        System.out.println(areAnagram(s1.toCharArray(), s2.toCharArray()));
    }
}
