package gfgmustdo.dp;

/**
 * Ques - Given a set of non-negative integers, and a value sum,
 * determine if there is a subset of the given set with sum equal to given sum.
 */
public class SubsetSum {

    static boolean isSubset(int[] arr, int sum) {
        return helper(arr, sum, arr.length);
    }

    private static boolean helper(int[] arr, int sum, int length) {
        if (length == 0 && sum != 0) {
            return false;
        }

        if (sum == 0) {
            return true;
        }

        if (arr[length - 1] > sum) {
            return helper(arr, sum, length - 1);
        }

        return helper(arr, sum, length - 1) || helper(arr, sum - arr[length - 1], length - 1);
    }

    public static void main(String[] args) {
        System.out.println(isSubset(new int[]{3, 34, 4, 12, 5, 2}, 27));
    }
}
