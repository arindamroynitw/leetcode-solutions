package gfgmustdo.dp;

public class MinInsertPalindrome {

    static int minimumInsertionsToFormPalindrome(String s) {
        return helper(s, 0, s.length() - 1);
    }

    private static int helper(String s, int low, int high) {
        if (low > high) {
            return Integer.MAX_VALUE;
        }

        if (low == high) {
            return 0;
        }

        if (low == high - 1) {
            return s.charAt(low) == s.charAt(high) ? 0 : 1;
        }


        if (s.charAt(low) == s.charAt(high)) {
            return helper(s, low + 1, high - 1);
        }

        return Math.min(helper(s, low, high - 1), helper(s, low + 1, high)) + 1;
    }

    private static int minimumInsertionsToFormPalindromeDp(String s) {

        // Create a table of size n*n. table[i][j]
        // will store minumum number of insertions
        // needed to convert str[i..j] to a palindrome.
        int[][] table = new int[s.length()][s.length()];
        int l, h, gap;

        // Fill the table
        for (gap = 1; gap < s.length(); ++gap) {

            for (l = 0, h = gap; h < s.length(); ++l, ++h) {

                table[l][h] = (s.charAt(l) == s.charAt(h)) ?
                        table[l + 1][h - 1] : (Integer.min(table[l][h - 1],
                        table[l + 1][h]) + 1);
            }

        }
        // Return minimum number of insertions
        // for str[0..n-1]
        return table[0][s.length() - 1];
    }


    public static void main(String[] args) {
        System.out.println(minimumInsertionsToFormPalindromeDp("abc"));
    }

}
