package gfgmustdo.dp;

/**
 * Ques - Given a value N, find the number of ways to make change for N cents,
 * if we have infinite supply of each of S = { S1, S2, .. , Sm} valued coins.
 * The order of coins doesn’t matter.
 * For example, for N = 4 and S = {1,2,3},
 * there are four solutions: {1,1,1,1},{1,1,2},{2,2},{1,3}. So output should be 4.
 * For N = 10 and S = {2, 5, 3, 6}, there are five solutions:
 * {2,2,2,2,2}, {2,2,3,3}, {2,2,6}, {2,3,5} and {5,5}. So the output should be 5.
 * <p>
 * <p>
 * <p>
 * Ref - https://www.geeksforgeeks.org/coin-change-dp-7/
 */
public class CoinChange {

    static int coinChangeDp(int[] arr, int target) {

        int[][] temp = new int[arr.length + 1][target + 1];

        for (int i = 0; i <= arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                if (j == 0) {
                    temp[i][j] = 1;
                }
                if (i == 0 && j > 0) {
                    temp[i][j] = 0;
                }
            }
        }


        for (int i = 1; i <= arr.length; i++) {
            for (int j = 1; j <= target; j++) {
                if (arr[i - 1] > j) {
                    temp[i][j] = temp[i - 1][j];
                } else {
                    temp[i][j] = temp[i - 1][j] + temp[i][j - arr[i - 1]];
                }
            }
        }

        return temp[arr.length][target];
    }

    static int coinChange(int[] arr, int target) {
        return helper(arr, target, arr.length);
    }

    private static int helper(int[] arr, int target, int length) {

        if (length < 0) {
            return 0;
        }

        if (target == 0) {
            return 1;
        }

        if (target > 0 && length <= 0) {
            return 0;
        }

        if (arr[length - 1] > target) {
            return helper(arr, target, length - 1);
        }

        return helper(arr, target, length - 1) + helper(arr, target - arr[length - 1], length);
    }

    public static void main(String[] args) {
        System.out.println(coinChange(new int[]{2, 5, 3, 6}, 5));
        System.out.println(coinChangeDp(new int[]{2, 5, 3, 6}, 5));

    }

}
