package gfgmustdo.dp;

/**
 *
 * Refs - https://www.youtube.com/watch?v=We3YDTzNXEk
 * Video - https://www.geeksforgeeks.org/edit-distance-dp-5/
 *
 */
public class EditDistance {

    static int editDistanceDynamic(String s1, String s2) {

        int m = s1.length();
        int n = s2.length();

        int[][] temp = new int[m + 1][n + 1];

        for (int i = 0; i <= m; i++) {
            temp[i][0] = i;
        }

        for (int j = 0; j <= n; j++) {
            temp[0][j] = j;
        }

        for (int i = 1; i <= m; i++) {
            for (int j = 1; j <= n; j++) {
                if (s1.charAt(i - 1) == s2.charAt(j - 1)) {
                    temp[i][j] = temp[i - 1][j - 1];
                } else {
                    temp[i][j] = 1 + Math.min(Math.min(temp[i - 1][j - 1], temp[i - 1][j]), temp[i][j - 1]);
                }

            }
        }

        return temp[m][n];
    }

    static int editDistance(String s1, String s2) {
        return helper(s1, s2, s1.length(), s2.length());
    }

    private static int helper(String s1, String s2, int l1, int l2) {

        if (l1 == 0) {
            return l2;
        }

        if (l2 == 0) {
            return l1;
        }

        if (s1.charAt(l1 - 1) == s2.charAt(l2 - 1)) {
            return helper(s1, s2, l1 - 1, l2 - 1);
        }

        int replace = helper(s1, s2, l1 - 1, l2 - 1);
        int insert = helper(s1, s2, l1, l2 - 1);
        int remove = helper(s1, s2, l1 - 1, l2);

        return Math.min(Math.min(insert, remove), replace) + 1;

    }

    public static void main(String[] args) {
        System.out.println(editDistance("abdc", "pqrst"));
        System.out.println(editDistanceDynamic("abdc", "pqrst"));

    }
}
