package gfgmustdo.dp;

import java.util.Arrays;

/**
 * Ques - Partition problem is to determine whether a given set
 * can be partitioned into two subsets such that the sum of elements in both subsets is same.
 */
public class SameSumPartition {

    static boolean isPartitionPossible(int[] arr) {

        int x = Arrays.stream(arr).sum();

        if (x % 2 != 0) {
            return false;
        }

        return SubsetSum.isSubset(arr, x / 2);
    }


    public static void main(String[] args) {

        System.out.println(isPartitionPossible(new int[]{3, 1, 5, 9, 12}));
    }
}
