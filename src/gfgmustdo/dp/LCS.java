package gfgmustdo.dp;

public class LCS {

    static int lcs(String s1, String s2) {
        char[] arr1 = s1.toCharArray();
        char[] arr2 = s2.toCharArray();

        return helper(arr1, arr2, arr1.length, arr2.length);

    }

    private static int helper(char[] arr1, char[] arr2, int length1, int length2) {

        if (length1 == 0 || length2 == 0) {
            return 0;
        }

        if (arr1[length1 - 1] == arr2[length2 - 1]) {
            return 1 + helper(arr1, arr2, length1 - 1, length2 - 1);
        } else {
            return Math.max(helper(arr1, arr2, length1 - 1, length2), helper(arr1, arr2, length1, length2 - 1));
        }
    }


}
