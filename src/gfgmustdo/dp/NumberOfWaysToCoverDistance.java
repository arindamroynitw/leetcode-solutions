package gfgmustdo.dp;

import java.util.Arrays;

public class NumberOfWaysToCoverDistance {

    static int printCountRec(int dist) {
        int[] arr = new int[dist + 1];
        Arrays.fill(arr, -1);

        arr[dist] = 0;
        return helper(0, dist, arr);
    }

    private static int helper(int start, int end, int[] arr) {
        if (start > end) {
            return 0;
        }

        if (start == end) {
            return 1;
        }

        if (arr[start] != -1) {
            return arr[start];
        }

        return helper(start + 1, end, arr) + helper(start + 2, end, arr) + helper(start + 3, end, arr);
    }

    public static void main(String[] args) {
        System.out.println(printCountRec(4));
    }
}
