package gfgmustdo.dp;

public class EggDropping {

    static int minAttempts(int floors, int eggs) {

        if (floors < 2) {
            return floors;
        }

        if (eggs == 1) {
            return floors;
        }

        int result = Integer.MAX_VALUE;
        for (int i = 1; i <= floors; i++) {
            int curr = Math.max(minAttempts(i - 1, eggs - 1), minAttempts(floors - i, eggs));

            if (curr < result) {
                result = curr;
            }
        }

        return result + 1; // 1 for the drop from the ith floor
    }

    static int minAttemptsDynamic(int floors, int eggs) {
        int[][] result = new int[floors + 1][eggs + 1];

        for (int i = 1; i <= floors; i++) {
            for (int j = 1; j <= eggs; j++) {
                if (i == 1) {
                    result[i][j] = i;
                } else if (j == 1) {
                    result[i][j] = i;
                } else {
                    int min = Integer.MAX_VALUE;
                    for (int x = 1; x <= i; x++) {
                        int curr = 1 + Math.max(result[x - 1][j - 1], result[i - x][j]);
                        if (curr < min) {
                            min = curr;
                        }
                    }
                    result[i][j] = min;
                }
            }
        }

        return result[floors][eggs];
    }

    public static void main(String[] args) {
        int n = 2, k = 30;
        System.out.print("Minimum number of "
                + "trials in worst case with "
                + n + " eggs and " + k
                + " floors is " + minAttempts(k, n));
    }
}
