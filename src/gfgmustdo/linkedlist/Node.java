package gfgmustdo.linkedlist;

class Node {

    public int data;
    public Node next;

    public Node(int data) {
        this.data = data;
    }

    static void print(Node head) {

        if (head == null) return;
        System.out.print(head.data + " ");
        print(head.next);

    }

    public static void main(String[] args) {
        Node head = new Node(1);
        head.next = new Node(2);
        head.next.next = new Node(3);
        head.next.next.next = new Node(4);
        head.next.next.next.next = new Node(5);

        print(head);
    }

    public static Node getStandardList() {
        Node head = new Node(1);
        head.next = new Node(2);
        head.next.next = new Node(3);
        head.next.next.next = new Node(4);
        head.next.next.next.next = new Node(5);
        return head;
    }
}
