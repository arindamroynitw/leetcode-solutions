package gfgmustdo.linkedlist;

public class ReverseLL {

    static Node reverseLL(Node head) {

        Node prev = null, curr = head, next = null;

        while (curr != null) {
            next = curr.next;
            curr.next = prev;
            prev = curr;
            curr = next;
        }

        return prev;
    }

    public static void main(String[] args) {
        Node ll = Node.getStandardList();

        Node.print(ll);

        ll = reverseLL(ll);

        Node.print(ll);
    }
}
