package gfgmustdo.linkedlist;

public class LastNthNode {

    static Node lastNth(Node head, int n) {

        Node slow = head, fast = head;

        for (int i = 0; i < n; i++) {
            if (fast == null) {
                return new Node(-1);
            }
            fast = fast.next;
        }

        while (fast != null) {
            fast = fast.next;
            slow = slow.next;
        }

        return slow;
    }

    public static void main(String[] args) {
        Node ll = Node.getStandardList();

        System.out.println(lastNth(ll, 6).data);
    }
}
