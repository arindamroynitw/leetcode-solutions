package gfgmustdo.heaps;

import java.util.ArrayDeque;
import java.util.Deque;

public class SlideWIndMax {

    static void slidWindMax(int[] arr, int k) {

        Deque<Integer> queue = new ArrayDeque<>();
        int i;
        for (i = 0; i < k; i++) {
            while (!queue.isEmpty() && arr[queue.peekLast()] <= arr[i]) {
                queue.removeLast();
            }

            if (queue.isEmpty() || arr[queue.peekLast()] > arr[i]) {
                queue.addLast(i);
            }
        }

        for (; i < arr.length; i++) {
            System.out.println("Current = " + arr[queue.peekFirst()]);

            while (!queue.isEmpty() && queue.peekFirst() <= i - k) {
                queue.removeFirst();
            }

            while (!queue.isEmpty() && arr[queue.peekLast()] <= arr[i]) {
                queue.removeLast();
            }

            if (queue.isEmpty() || arr[queue.peekLast()] > arr[i]) {
                queue.addLast(i);
            }
        }

        if (!queue.isEmpty()) {
            System.out.println("Current = " + arr[queue.peekFirst()]);
        }
    }

    public static void main(String[] args) {
        slidWindMax(new int[]{2, 7, 6, 5, 1, 2, 8, 100, 2}, 3);
    }

}
