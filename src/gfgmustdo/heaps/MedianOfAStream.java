package gfgmustdo.heaps;

import java.util.Arrays;
import java.util.Collections;
import java.util.PriorityQueue;
import java.util.stream.Collectors;

public class MedianOfAStream {


    public static double[] medianOfStream(int[] input) {

        PriorityQueue<Integer> lowers = new PriorityQueue<>(Collections.reverseOrder());
        PriorityQueue<Integer> uppers = new PriorityQueue<>();

        double[] medians = new double[input.length];

        for (int i = 0; i < input.length; i++) {
            int number = input[i];
            addNumber(number, uppers, lowers);
            rebalance(uppers, lowers);
            medians[i] = calculateMedian(uppers, lowers);
        }

        return medians;
    }

    private static double calculateMedian(PriorityQueue<Integer> uppers, PriorityQueue<Integer> lowers) {
        PriorityQueue<Integer> biggerHeap = lowers.size() > uppers.size() ? lowers : uppers;
        PriorityQueue<Integer> smallerHeap = lowers.size() > uppers.size() ? uppers : lowers;

        if (biggerHeap.size() == smallerHeap.size()) {
            return ((double) (biggerHeap.peek() + smallerHeap.peek())) / 2;
        } else {
            return biggerHeap.peek();
        }
    }

    private static void rebalance(PriorityQueue<Integer> uppers, PriorityQueue<Integer> lowers) {
        PriorityQueue<Integer> biggerHeap = lowers.size() > uppers.size() ? lowers : uppers;
        PriorityQueue<Integer> smallerHeap = lowers.size() > uppers.size() ? uppers : lowers;

        if (biggerHeap.size() - smallerHeap.size() >= 2) {
            smallerHeap.add(biggerHeap.poll());
        }
    }

    private static void addNumber(int number, PriorityQueue<Integer> uppers, PriorityQueue<Integer> lowers) {
        if (lowers.isEmpty() || lowers.peek() > number) {
            lowers.add(number);
        } else {
            uppers.add(number);
        }
    }


    public static void main(String[] args) {
        double[] medians = medianOfStream(new int[]{2, 3, 7, 1, 5, 4, 6});

        Arrays.stream(medians).boxed().collect(Collectors.toList()).stream().forEach(System.out::println);
    }
}
