package gfgmustdo.trees;

/**
 * Video ref = https://www.youtube.com/watch?v=MILxfAbIhrE
 */
public class CheckIfBST {

    static boolean checkIfBst(Node root, int min, int max) {
        if (root == null) {
            return true;
        }

        if (root.data > max || root.data < min) {
            return false;
        }

        return checkIfBst(root.left, min, root.data) && checkIfBst(root.right, root.data, max);
    }

    public static void main(String[] args) {
        Node root = new Node(4);
        root.left = new Node(2);
        root.right = new Node(5);
        root.left.left = new Node(1);
        root.left.right = new Node(3);

        if (checkIfBst(root, Integer.MIN_VALUE, Integer.MAX_VALUE))
            System.out.println("IS BST");
        else
            System.out.println("Not a BST");
    }
}

