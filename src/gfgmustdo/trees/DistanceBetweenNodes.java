package gfgmustdo.trees;

public class DistanceBetweenNodes {

    static int distBetweenNodes(Node root, int n1, int n2) {

        Node lca = LowestCommonAncestor.lca(root, n1, n2);

        int left = LevelOfNode.level(lca, n1);
        int right = LevelOfNode.level(lca, n2);

        return (left - 1) + (right - 1);
    }

    public static void main(String[] args) {
        System.out.println(distBetweenNodes(Node.getStandardTree(), 2, 5));
    }
}
