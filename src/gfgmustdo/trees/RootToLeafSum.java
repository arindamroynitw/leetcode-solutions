package gfgmustdo.trees;

public class RootToLeafSum {

    static boolean rootToLeafSum(Node root, int sum) {
        if (root == null) {
            return false;
        }

        if (Node.isLeaf(root) && root.data == sum) {
            return true;
        }

        return rootToLeafSum(root.left, sum - root.data) || rootToLeafSum(root.right, sum - root.data);

    }

    public static void main(String[] args) {
        Node root = Node.getStandardTree();
        System.out.println(rootToLeafSum(root, 11));
        System.out.println(rootToLeafSum(root, 5));
    }
}
