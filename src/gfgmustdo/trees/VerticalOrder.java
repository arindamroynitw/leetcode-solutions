package gfgmustdo.trees;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *         1
 *         /    \
 *        2      3
 *       / \   /   \
 *      4   5  6   7
 *                /  \
 *               8   9
 *
 *
 * The output of print this tree vertically will be:
 * 4
 * 2
 * 1 5 6
 * 3 8
 * 7
 * 9
 */
public class VerticalOrder {


    private static void printVerticalOrder(Node root) {

        Map<Integer, List<Node>> columnToNodesMap = new TreeMap<>();
        int horizontalDistance = 0;

        getVerticalOrder(root, horizontalDistance, columnToNodesMap);

        columnToNodesMap.forEach((key, value) -> System.out.println(key + " - " + value));
    }

    private static void getVerticalOrder(Node node, int horizontalDistance, Map<Integer, List<Node>> columnToNodesMap) {

        if(node==null) {
            return;
        }
        List<Node> curr = columnToNodesMap.get(horizontalDistance);

        if (curr == null) {
            curr = new ArrayList<>();
            curr.add(node);
            columnToNodesMap.put(horizontalDistance, curr);
        } else {
            curr.add(node);
        }

        getVerticalOrder(node.left, horizontalDistance - 1, columnToNodesMap);
        getVerticalOrder(node.right, horizontalDistance + 1, columnToNodesMap);

    }


    public static void main(String[] args) {
        Node root = Node.getStandardTree();

        printVerticalOrder(root);
    }


}
