package gfgmustdo.trees;

/**
 * Given a Binary Tree, print left view of it.
 * Left view of a Binary Tree is set of nodes visible when tree is visited from left side.
 */
public class LeftView {

    static int max_level = 0;

    static void leftView(Node root, int level) {
        if (root == null) {
            return;
        }
        if (level > max_level) {
            System.out.print(root.data + " - ");
            max_level = level;
        }
        leftView(root.left, level + 1);
        leftView(root.right, level + 1);
    }

    public static void main(String[] args) {
        Node root = Node.getStandardTree();

        leftView(root, 1);
    }
}
