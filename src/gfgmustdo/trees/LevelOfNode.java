package gfgmustdo.trees;

public class LevelOfNode {

    public static int level(Node root, int x) {

        if (root == null) {
            return 0;
        }

        if (root.data == x) {
            return 1;
        }

        if (level(root.left, x) != 0) {
            return 1 + level(root.left, x);
        } else {
            return 1 + level(root.right, x);
        }
    }

    public static void main(String[] args) {
        Node root = Node.getStandardTree();

        System.out.println(level(root,7));
    }

}
