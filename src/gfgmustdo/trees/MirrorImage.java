package gfgmustdo.trees;

public class MirrorImage {

    static void mirrorImage(Node root) {
        if (root == null) {
            return;
        }

        mirrorImage(root.left);
        mirrorImage(root.right);

        Node temp = root.left;
        root.left = root.right;
        root.right = temp;
    }


    public static void main(String[] args) {
        Node root = Node.getStandardTree();
        TreeUtils.preOrder(root);
        System.out.println();
        mirrorImage(root);

        TreeUtils.preOrder(root);
    }

}
