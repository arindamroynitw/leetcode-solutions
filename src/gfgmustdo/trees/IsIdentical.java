package gfgmustdo.trees;

public class IsIdentical {

    static boolean isIdentical(Node n1, Node n2) {
        if (n1 == null && n2 == null) {
            return true;
        }

        if (n1 != null && n2 != null && n1.data == n2.data) {
            return isIdentical(n1.left, n2.left) && isIdentical(n1.right, n2.right);
        }
        return false;
    }

    public static void main(String[] args) {
        Node n1 = Node.getStandardTree();
        Node n2 = Node.getStandardTree();
//        n2.right.right.data = 10;

        System.out.println(isIdentical(n1, n2));
    }
}
