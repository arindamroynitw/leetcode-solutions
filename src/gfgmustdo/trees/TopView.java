package gfgmustdo.trees;

import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.TreeMap;

public class TopView {


    public static void main(String[] args) {
//        topView(Node.getStandardTree());


        Node root = new Node(1);
        root.left = new Node(2);
        root.right = new Node(3);
        root.left.right = new Node(4);
        root.left.right.right = new Node(5);
        root.left.right.right.right = new Node(6);

        topView(root);
    }

    private static void topView(Node root) {
        Map<Integer, Node> topViewMap = new TreeMap<>();
        fillTopViewMap(root, topViewMap);

        topViewMap.forEach((key, value) -> {
            System.out.println(value);
        });
    }

    private static void fillTopViewMap(Node root, Map<Integer, Node> topViewMap) {

        class Cust {
            public Node node;
            public int horizontalDistance;

            public Cust(Node node, int horizontalDistance) {
                this.node = node;
                this.horizontalDistance = horizontalDistance;
            }
        }

        Queue<Cust> queue = new LinkedList<>();
        queue.add(new Cust(root, 0));

        while (!queue.isEmpty()) {
            Cust curr = queue.poll();

            topViewMap.putIfAbsent(curr.horizontalDistance, curr.node);

            if (curr.node.left != null) {
                queue.add(new Cust(curr.node.left, curr.horizontalDistance - 1));
            }
            if (curr.node.right != null) {
                queue.add(new Cust(curr.node.right, curr.horizontalDistance + 1));
            }
        }

    }
}
