package gfgmustdo.trees;

/**
 * Video ref - https://www.youtube.com/watch?v=ey7DYc9OANo
 */
public class Diameter {

    private static int height(Node node) {
        if (node == null) {
            return 0;
        }

        return Math.max(height(node.left), height(node.right)) + 1;
    }

    public static int diameter(Node root) {
        if (root == null) {
            return 0;
        }

        int diameter = height(root.left) + height(root.right) + 1;

        int leftDiameter = diameter(root.left);

        int rightDiameter = diameter(root.right);

        return Math.max(diameter,Math.max(leftDiameter,rightDiameter));
    }

    public static void main(String[] args) {
        System.out.println(diameter(Node.getStandardTree()));
    }

}
