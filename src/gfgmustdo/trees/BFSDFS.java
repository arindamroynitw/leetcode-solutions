package gfgmustdo.trees;

import java.util.ArrayDeque;
import java.util.Deque;

public class BFSDFS {

    static void dfs(Node root) {
        if(root == null) {
            return;
        }

        System.out.print(root.data + " - ");
        dfs(root.left);
        dfs(root.right);
    }

    static void bfs(Node root) {
        if (root == null) {
            return;
        }

        Deque<Node> queue = new ArrayDeque<>();
        queue.add(root);

        while (!queue.isEmpty()) {
            Node curr = queue.remove();
            System.out.print(curr.data + " - ");
            if (curr.left != null) {
                queue.add(curr.left);
            }
            if (curr.right != null) {
                queue.add(curr.right);
            }
        }

    }

    public static void main(String[] args) {
        bfs(Node.getStandardTree());
        System.out.println();
        dfs(Node.getStandardTree());
    }
}
