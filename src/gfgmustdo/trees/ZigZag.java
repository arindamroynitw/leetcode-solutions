package gfgmustdo.trees;

import java.util.Deque;
import java.util.LinkedList;
import java.util.Stack;

public class ZigZag {

    static void twoStack(Node root) {

        if(root == null) {
            return;
        }

        Stack<Node> s1 = new Stack<>(), s2 = new Stack<>();

        s1.push(root);

        while (!s1.isEmpty() || !s2.isEmpty()) {

            while (!s1.isEmpty()) {
                Node curr = s1.pop();
                if (curr.left != null) {
                    s2.push(curr.left);
                }
                if (curr.right != null) {
                    s2.push(curr.right);
                }
                System.out.print(curr.data + " ");
            }

            while (!s2.isEmpty()) {
                Node curr = s2.pop();
                if (curr.right != null) {
                    s1.push(curr.right);
                }
                if (curr.left != null) {
                    s1.push(curr.left);
                }
                System.out.print(curr.data + " ");
            }
        }

    }


    static void oneQueue(Node root) {

        if (root == null) {
            return;
        }
        Deque<Node> deque = new LinkedList<Node>();
        deque.offerFirst(root);
        int count = 1;
        boolean flip = true;
        while (!deque.isEmpty()) {
            int currentCount = 0;
            while (count > 0) {
                if (flip) {
                    root = deque.pollFirst();
                    System.out.print(root.data + " ");
                    if (root.left != null) {
                        deque.offerLast(root.left);
                        currentCount++;
                    }
                    if (root.right != null) {
                        deque.offerLast(root.right);
                        currentCount++;
                    }
                } else {
                    root = deque.pollLast();
                    System.out.print(root.data + " ");
                    if (root.right != null) {
                        deque.offerFirst(root.right);
                        currentCount++;
                    }
                    if (root.left != null) {
                        deque.offerFirst(root.left);
                        currentCount++;
                    }
                }
                count--;
            }
            flip = !flip;
            count = currentCount;
        }

    }

    public static void main(String[] args) {
        twoStack(Node.getStandardTree());
    }
}
