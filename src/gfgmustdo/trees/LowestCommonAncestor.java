package gfgmustdo.trees;

/**
 * Video ref - https://www.youtube.com/watch?v=13m9ZCB8gjw
 */
public class LowestCommonAncestor {
//
//    static Node lca(Node root, Node n1, Node n2) {
//        if (root == null) {
//            return null;
//        }
//        if (root == n1 || root == n2) {
//            return root;
//        }
//
//        Node left = lca(root.left, n1, n2);
//        Node right = lca(root.right, n1, n2);
//
//        if (left != null && right != null) {
//            return root;
//        }
//        if (left == null && right == null) {
//            return null;
//        }
//
//        return left != null ? left : right;
//    }

    static Node lca(Node root, int n1, int n2) {
        if (root == null) {
            return null;
        }
        if (root.data == n1 || root.data == n2) {
            return root;
        }

        Node left = lca(root.left, n1, n2);
        Node right = lca(root.right, n1, n2);

        if (left != null && right != null) {
            return root;
        }
        if (left == null && right == null) {
            return null;
        }

        return left != null ? left : right;
    }

    public static void main(String[] args) {
        System.out.println(lca(Node.getStandardTree(),4,10).data);
    }
}
