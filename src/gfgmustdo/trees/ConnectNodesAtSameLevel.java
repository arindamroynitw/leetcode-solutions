package gfgmustdo.trees;

import java.util.LinkedList;
import java.util.Queue;

class ModifiedNode {

    public int data;

    public ModifiedNode left;

    public ModifiedNode right;

    public ModifiedNode next;

    public ModifiedNode(int data) {
        this.data = data;
    }
}

public class ConnectNodesAtSameLevel {


    public static void main(String[] args) {
        ModifiedNode root = new ModifiedNode(1);
        root.left = new ModifiedNode(2);
        root.right = new ModifiedNode(3);
        root.left.left = new ModifiedNode(4);
        root.left.right = new ModifiedNode(5);
        root.right.left = new ModifiedNode(6);
        root.right.right = new ModifiedNode(7);


        connectNodesAtSameLevel(root);

        root = root;
    }

    private static void connectNodesAtSameLevel(ModifiedNode root) {
        Queue<ModifiedNode> queue = new LinkedList<>();
        queue.add(root);

        // null marker to represent end of current level
        queue.add(null);

        // Do Level order of tree using NULL markers
        while (!queue.isEmpty()) {
            ModifiedNode curr = queue.peek();
            queue.remove();
            if (curr != null) {

                // next element in queue represents next
                // node at current Level
                curr.next = queue.peek();

                // push left and right children of current
                // node
                if (curr.left != null)
                    queue.add(curr.left);
                if (curr.right != null)
                    queue.add(curr.right);
            }

            // if queue is not empty, push NULL to mark
            // nodes at this level are visited
            else if (!queue.isEmpty())
                queue.add(null);
        }
    }
}
