package gfgmustdo.trees;

public class LeafNodes {

    static int leafNodes(Node root) {

        if (root == null) {
            return 0;
        }

        if (root.left == null && root.right == null) {
            return 1;
        }

        int left = 0, right = 0;
        if (root.left != null) {
            left = leafNodes(root.left);
            right = leafNodes(root.right);
        }

        return left + right;
    }

    public static void main(String[] args) {
        System.out.println(leafNodes(Node.getStandardTree()));

        Node root = new Node(1);
        root.left = new Node(2);
        root.right = new Node(3);
        root.left.left = new Node(4);
        root.left.right = new Node(5);

        System.out.println(leafNodes(root));
    }
}
