package gfgmustdo.queue;

public class StackUsingQueue {

    MyQueue q1, q2;

    public StackUsingQueue() {
        q1 = new MyQueue();
        q2 = new MyQueue();
    }

    public void push(int x) {
        if (q1.isEmpty()) {
            q1.enQueue(x);
            return;
        }

        q2.enQueue(x);

        while (!q1.isEmpty()) {
            q2.enQueue(q1.deQueue());
        }

        MyQueue temp = q1;
        q1 = q2;
        q2 = temp;
    }

    public int pop() {
        if(q1.isEmpty()) {
            return -1;
        }

        return q1.deQueue();
    }

    public static void main(String[] args) {
        StackUsingQueue stack = new StackUsingQueue();

        stack.push(1);
        stack.push(2);
        stack.push(3);
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        stack.push(4);
        System.out.println(stack.pop());
    }
}
