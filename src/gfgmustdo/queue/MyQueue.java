package gfgmustdo.queue;

public class MyQueue {

    Node front, rear;

    public MyQueue() {
        front = null;
        rear = null;
    }

    public void enQueue(int x) {
        Node temp = new Node(x);
        if (front == null) {
            front = temp;
            rear = front;
            return;
        }

        rear.next = temp;
        rear = temp;
    }

    public int deQueue() {
        Node temp = front;
        if (temp == null) {
            return -1;
        }
        front = front.next;
        if (front == null) {
            rear = null;
        }

        return temp.data;
    }

    public boolean isEmpty() {
        return front == null;
    }


    public static void main(String[] args) {
        MyQueue queue = new MyQueue();
        queue.enQueue(1);
        queue.enQueue(2);
        queue.enQueue(3);
        System.out.println(queue.deQueue());
        System.out.println(queue.deQueue());
        queue.enQueue(4);
        System.out.println(queue.deQueue());
    }
}
