package gfgmustdo.queue;

public class CircularTour {

    // A petrol pump has petrol and distance to next petrol pump
    static class petrolPump {
        int petrol;
        int distance;

        // constructor
        public petrolPump(int petrol, int distance) {
            this.petrol = petrol;
            this.distance = distance;
        }
    }

    // The function returns starting point if there is a possible solution,
    // otherwise returns -1
    static int printTour(petrolPump arr[], int n) {

        int start = 0, end = 1;

        int curr_petrol = arr[start].petrol - arr[start].distance;

        while (start != end || curr_petrol < 0) {

            while (start != end && curr_petrol < 0) {
                curr_petrol -= arr[start].petrol - arr[start].distance;
                start = (start + 1) % n;

                if (start == 0) {
                    return -1;
                }
            }

            curr_petrol += arr[end].petrol - arr[end].distance;

            end = (end + 1) % n;
        }

        return start;
    }

    // Driver program to test above functions
    public static void main(String[] args) {

        petrolPump[] arr = {new petrolPump(6, 4),
                new petrolPump(3, 6),
                new petrolPump(7, 3)};

        int start = printTour(arr, arr.length);

        System.out.println(start == -1 ? "No Solution" : "Start = " + start);

    }

}
