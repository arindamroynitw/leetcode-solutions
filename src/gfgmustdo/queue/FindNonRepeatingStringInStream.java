package gfgmustdo.queue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class FindNonRepeatingStringInStream {

    final static String NOT_FOUND_OUTPUT = "*";


    static void findFirstNonRepeatingInStream(String[] input) {
        List<String> isCurrentlyInDLL = new ArrayList<>();

        Map<String, Boolean> isVisited = new HashMap<>();

        for (int i = 0; i < input.length; i++) {

            String curr = input[i];

            if (!isVisited.containsKey(curr)) {

                if (!isCurrentlyInDLL.contains(curr)) {
                    isCurrentlyInDLL.add(curr);
                }

                isVisited.put(curr, true);


            } else {
                isCurrentlyInDLL.remove(curr);
                isVisited.put(curr, true);
            }


            if (isCurrentlyInDLL.size() == 0) {
                System.out.println(NOT_FOUND_OUTPUT);
            } else {
                System.out.println(isCurrentlyInDLL.get(0));
            }

        }


    }

    public static void main(String[] args) {
        String[] input = new String[]{"ab", "ab", "a", "b", "b", "a", "c"};
        findFirstNonRepeatingInStream(input);
    }
}