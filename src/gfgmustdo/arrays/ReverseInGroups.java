package gfgmustdo.arrays;

public class ReverseInGroups {

    static void reverse(int arr[], int k) {
        int start = 0, end = k, rem = arr.length;

        while (rem > k) {
            for (int i = start, j = end - 1; i < j; i++, j--) {
                int temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
            start = end;
            end = end + k;
            rem = rem - k;
        }

        end = arr.length;

        for (int i = start, j = end - 1; i < j; i++, j--) {
            int temp = arr[i];
            arr[i] = arr[j];
            arr[j] = temp;
        }
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};

        reverse(arr, 5);

        for (int i : arr) {
            System.out.print(i + " - ");

        }
    }
}
