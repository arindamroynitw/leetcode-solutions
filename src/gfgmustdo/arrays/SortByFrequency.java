package gfgmustdo.arrays;

import java.util.HashMap;
import java.util.Map;

public class SortByFrequency {

    static int[] sort(int[] arr) {

        Map<Integer, Integer> map = new HashMap<>();

        for (int i = 0; i < arr.length; i++) {
            if (map.containsKey(arr[i])) {
                map.put(arr[i], map.get(arr[i] + 1));
            } else {
                map.put(arr[i], 1);
            }
        }

        //sort it later

        return null;
    }
}
