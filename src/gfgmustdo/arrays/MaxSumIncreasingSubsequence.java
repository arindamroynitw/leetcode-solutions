package gfgmustdo.arrays;

/**
 * Given an array of n positive integers. Write a program to find the sum of maximum sum subsequence
 * of the given array such that the intgers in the subsequence are sorted in increasing order.
 * For example,
 * if input is {1, 101, 2, 3, 100, 4, 5},
 * then output should be 106 (1 + 2 + 3 + 100),
 * if the input array is {3, 4, 5, 10},
 * then output should be 22 (3 + 4 + 5 + 10) and
 * if the input array is {10, 5, 4, 3},
 * then output should be 10
 */
public class MaxSumIncreasingSubsequence {

    static int msisDynamic(int[] arr) {

        int temp[] = new int[arr.length];

        for (int i = 0; i < arr.length; i++) {
            temp[i] = arr[i];
        }

        for (int i = 1; i < arr.length; i++) {
            for (int j = 0; j < i; j++) {
                if (arr[j] <= arr[i]) {
                    temp[i] = Math.max(temp[i], arr[i] + temp[j]);
                }
            }
        }

        int result = temp[0];
        for (int i = 0; i < temp.length; i++) {
            result = Math.max(result, temp[i]);
        }

        return result;
    }

    static int msis(int[] arr) {

        int max = Integer.MIN_VALUE;

        for (int i = 0; i < arr.length; i++) {
            int curr = helper(arr, i);
            if (curr > max) {
                max = curr;
            }
        }
        return max;
    }

    private static int helper(int[] arr, int pos) {

        if (pos == 0) {
            return arr[0];
        }

        int maxAti = Integer.MIN_VALUE;
        for (int i = 0; i < pos; i++) {
            if (arr[i] < arr[pos]) {
                maxAti = Math.max(maxAti, arr[pos] + helper(arr, i));
            }
        }

        return maxAti;
    }

    public static void main(String[] args) {
        System.out.println(msisDynamic(new int[]{2, 3}));
    }


}
