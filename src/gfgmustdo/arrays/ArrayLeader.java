package gfgmustdo.arrays;

/**
 * Write a program to print all the LEADERS in the array.
 * An element is leader if it is greater than all the elements to its right side.
 * And the rightmost element is always a leader.
 * For example int the array {16, 17, 4, 3, 5, 2}, leaders are 17, 5 and 2.
 */
public class ArrayLeader {

    static void leader(int[] arr) {

        int leader = arr[arr.length - 1];
        System.out.println(leader);

        for (int i = arr.length - 2; i >= 0; i--) {
            if (arr[i] > leader) {
                leader = arr[i];
                System.out.println(leader);
            }
        }

    }

    public static void main(String[] args) {
        leader(new int[]{16, 17, 4, 3, 5, 2});
    }
}
