package gfgmustdo.arrays;

/**
 * You are given a list of n-1 integers and these integers are in the range of 1 to n. There are no duplicates in list.
 * One of the integers is missing in the list. Write an efficient code to find the missing integer.
 */
public class MissingElementInArray {

    /**
     * One simple way is n(n+1)/2 - sum, but that may cause overflow. Hence below method.
     *
     * @param arr
     * @return
     */
    static int missingElement(int[] arr) {

        int oneToNXor = 1, arrayXor = arr[0];

        for (int i = 2; i <= arr.length + 1; i++) {
            oneToNXor = oneToNXor ^ i;
        }

        for (int i = 1; i < arr.length; i++) {
            arrayXor = arrayXor ^ arr[i];
        }

        return oneToNXor ^ arrayXor;

    }

    public static void main(String[] args) {
        System.out.println(missingElement(new int[]{1, 2, 4, 5}));
    }
}
