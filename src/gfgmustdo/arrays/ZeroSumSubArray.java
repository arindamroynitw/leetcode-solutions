package gfgmustdo.arrays;

import java.util.HashMap;
import java.util.Map;

/**
 * Given an array of integers, find length of the largest subarray with sum equals to 0.
 */
public class ZeroSumSubArray {

    static int largestSubArrayLength(int[] arr) {

        Map<Integer, Integer> map = new HashMap<>();

        int sum = 0;
        int maxLen = 0;

        for (int i = 0; i < arr.length; i++) {

            sum = sum + arr[i];

            if (arr[0] == 0 && maxLen == 0) {
                maxLen = 1;
            }

            if (sum == 0) {
                maxLen = i + 1;
            }

            Integer prev_i = map.get(sum);

            if (prev_i != null) {
                maxLen = Math.max(maxLen, i - prev_i);
            } else {
                map.put(sum, i);
            }

        }

        return maxLen;

    }

    public static void main(String arg[]) {
        int arr[] = {15, -2, 2, -8, 1, 7, 10, 23};
        System.out.println("Length of the longest 0 sum subarray is "
                + largestSubArrayLength(arr));
    }
}
