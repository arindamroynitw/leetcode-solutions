package gfgmustdo.arrays;

/**
 * Only one element occurs once, rest occur twice. Find that element.
 */
public class NonRepeatingElement {

    public static void main(String[] args) {

        int result = 0;
        int[] arr = {1, 2, 3, 4, 4, 3, 1};
        for (int i : arr) {
            result ^= i;
        }

        System.out.println("Result is " + result);
    }
}
