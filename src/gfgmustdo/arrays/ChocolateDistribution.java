package gfgmustdo.arrays;

import java.lang.annotation.Native;
import java.util.Arrays;

/**
 * Given an array of n integers where each value represents number of chocolates in a packet. Each packet can have variable number of chocolates. There are m students, the task is to distribute chocolate packets such that:
 * <p>
 * Each student gets one packet.
 * The difference between the number of chocolates in packet with maximum chocolates and packet with minimum chocolates given to the students is minimum.
 * Examples:
 * <p>
 * Input : arr[] = {7, 3, 2, 4, 9, 12, 56}
 * m = 3
 * Output: Minimum Difference is 2
 * We have seven packets of chocolates and
 * we need to pick three packets for 3 students
 * If we pick 2, 3 and 4, we get the minimum
 * difference between maximum and minimum packet
 * sizes.
 * <p>
 * Input : arr[] = {3, 4, 1, 9, 56, 7, 9, 12}
 * m = 5
 * Output: Minimum Difference is 6
 * The set goes like 3,4,7,9,9 and the output
 * is 9-3 = 6
 * <p>
 * Input : arr[] = {12, 4, 7, 9, 2, 23, 25, 41,
 * 30, 40, 28, 42, 30, 44, 48,
 * 43, 50}
 * m = 7
 * Output:  Minimum Difference is 10
 * We need to pick 7 packets. We pick 40, 41,
 * 42, 44, 48, 43 and 50 to minimize difference
 * between maximum and minimum.
 */
public class ChocolateDistribution {


    static int findMinDiff(int[] arr, int m) {

        if (arr.length == 0 || m == 0) {
            return 0;
        }

        if (m > arr.length) {
            return -1;
        }

        Arrays.sort(arr);

        int start = 0;
        int end = start + m - 1;

        int res = Integer.MAX_VALUE;

        while (end < arr.length) {
            res = Math.min(res, arr[end] - arr[start]);
            start++;
            end++;
        }

        return res;
    }

    public static void main(String[] args) {
        int arr[] = {12, 4, 7, 9, 2, 23, 25, 41, 30, 40, 28, 42, 30, 44, 48, 43, 50};

        int m = 7;  // Number of students

        System.out.println("Minimum difference is "
                + findMinDiff(arr, m));

    }
}

