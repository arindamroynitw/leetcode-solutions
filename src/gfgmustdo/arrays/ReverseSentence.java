package gfgmustdo.arrays;

/**
 * Input  : s = "getting good at coding needs a lot of practice"
 * Output : s = "practice of lot a needs coding at good getting"
 * <p>
 * Algo -
 * Reverse the individual words, we get the below string.
 * "i ekil siht margorp yrev hcum"
 * 2) Reverse the whole string from start to end and you get the desired output.
 * "much very program this like i"
 */
public class ReverseSentence {

    static String reverse(String sentence) {

        if(sentence.length() == 0) {
            return sentence;
        }

        char[] res = sentence.toCharArray();

        int start = 0, end = 0;
        while (end < res.length) {
            if (res[end] == ' ') {
                reversePart(res, start, end - 1);
                start = end + 1;
                end = end + 1;
            } else if (end == res.length - 1) {
                reversePart(res, start, end);
                break;
            } else {
                end = end + 1;
            }
        }

        reversePart(res, 0, res.length - 1);
        return String.valueOf(res);

    }

    static void reversePart(char[] inp, int start, int end) {
        for (int i = start, j = end; i < j; i++, j--) {
            char temp = inp[i];
            inp[i] = inp[j];
            inp[j] = temp;
        }
    }

    public static void main(String[] args) {
        String s = "getting good at coding needs a lot of practice";

//        String s = "am boy";

        System.out.println(reverse(s));
    }

}
