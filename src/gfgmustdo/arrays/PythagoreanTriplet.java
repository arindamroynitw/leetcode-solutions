package gfgmustdo.arrays;

import java.util.Arrays;

public class PythagoreanTriplet {

    static boolean isTriplet(int[] arr) {

        int[] squares = new int[arr.length];

        for (int i = 0; i < arr.length; i++) {
            squares[i] = arr[i] * arr[i];
        }

        Arrays.sort(squares);

        for (int i = squares.length - 1; i >= 2; i--) {
            int target = squares[i];

            int start = 0, end = i - 1;

            while (start < end) {
                if (squares[start] + squares[end] == target) {
                    return true;
                } else if (squares[start] + squares[end] > target) {
                    end--;
                } else {
                    start++;
                }
            }
        }
        return false;
    }

    public static void main(String[] args) {
        int arr[] = {15, 2, 1, 4, 6, 8, 10};
        if (isTriplet(arr) == true)
            System.out.println("Yes");
        else
            System.out.println("No");
    }
}
