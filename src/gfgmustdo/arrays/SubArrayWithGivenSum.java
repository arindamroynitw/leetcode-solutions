package gfgmustdo.arrays;

/**
 * Find subarray with given sum | Set 1 (Nonnegative Numbers)
 * <p>
 * Given an unsorted array of nonnegative integers,
 * find a continous subarray which adds to a given number.
 */
public class SubArrayWithGivenSum {

    /**
     * Solution ref - https://www.geeksforgeeks.org/find-subarray-with-given-sum/
     * @param arr
     * @param target
     */
    static void subArrayWithGivenSum(int[] arr, int target) {

        int curr = arr[0], start = 0, i;

        for (i = 1; i <= arr.length; i++) {

            while (curr > target && start < i - 1) {
                curr = curr - arr[start];
                start++;
            }

            if (curr == target) {
                System.out.println(start + "--" + (i - 1));
                return;
            }

            if (i < arr.length) {
                curr = curr + arr[i];
            }
        }




    }

    public static void main(String[] args) {
        subArrayWithGivenSum(new int[]{1, 4, 20, 3, 10, 5}, 20);
    }

}
