package gfgmustdo.arrays;

import java.util.PriorityQueue;

public class KthLargestElementInAStream {

    static void KthLargestSoFar(int[] arr, int k) {
        PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();

        for (int number : arr) {

            priorityQueue.add(number);

            if (priorityQueue.size() < k) {
                continue;
            }

            if (priorityQueue.size() > k) {
                priorityQueue.poll();
            }

            if (priorityQueue.size() == k) {
                System.out.println("Current val :: " + priorityQueue.peek());
            }
        }
    }


    public static void main(String[] args) {
        KthLargestSoFar(new int[]{10, 20, 11, 70, 50, 40, 100, 5}, 3);
    }

}
