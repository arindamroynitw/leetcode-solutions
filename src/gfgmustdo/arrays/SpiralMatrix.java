package gfgmustdo.arrays;

public class SpiralMatrix {

    static void print(int[][] arr, int m, int n) {
        int top = 0, bottom = m - 1, left = 0, right = n - 1;

        int dir = 0; // 0 = right to left, 1 = top to bottom, 2 = left to right, 3 = bottom to top


        while (top <= bottom && left <= right) {
            switch (dir) {
                case 0:
                    for (int i = left; i <= right; i++) {
                        System.out.print(arr[top][i] + " ");
                    }
                    top++;
                    dir++;
                    break;
                case 1:
                    for (int j = top; j <= bottom; j++) {
                        System.out.print(arr[j][right] + " ");
                    }
                    right--;
                    dir++;
                    break;
                case 2:
                    for (int i = right; i >= left; i--) {
                        System.out.print(arr[bottom][i] + " ");
                    }
                    bottom--;
                    dir++;
                    break;
                case 3:
                    for (int j = bottom; j >= top; j--) {
                        System.out.println(arr[j][left]);
                    }
                    left++;
                    dir = 0;
                    break;
            }
        }

    }


    public static void main(String[] args) {
        int R = 3;
        int C = 6;
        int a[][] = {{1, 2, 3, 4, 5, 6},
                {7, 8, 9, 10, 11, 12},
                {13, 14, 15, 16, 17, 18}
        };
        print(a, R, C);
    }

}
