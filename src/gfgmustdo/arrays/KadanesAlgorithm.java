package gfgmustdo.arrays;

/**
 * Write an efficient program to find the sum of contiguous subarray within a one-dimensional
 * array of numbers which has the largest sum.
 */
public class KadanesAlgorithm {

    /**
     * Solution Algo -
     * <p>
     * Simple idea of the Kadane’s algorithm is to look for all
     * positive contiguous segments of the array (max_ending_here is used for this).
     * And keep track of maximum sum contiguous segment among all positive segments
     * (max_so_far is used for this). Each time we get a positive
     * sum compare it with max_so_far and update max_so_far if it is greater than max_so_far
     *
     * @param arr
     * @return
     */
    static int maxSubArraySum(int[] arr) {

        int max_so_far = 0;
        int max_ending_here = 0;

        for (int i = 0; i < arr.length; i++) {
            max_ending_here = max_ending_here + arr[i];
            if (max_ending_here < 0) {
                max_ending_here = 0;
            }

            if (max_ending_here > max_so_far) {
                max_so_far = max_ending_here;
            }
        }
        return max_so_far;

    }

    public static void main(String[] args) {
        System.out.println(maxSubArraySum(new int[]{-2, -3, 4, -1, -2, 1, 5, -3}));
    }


}
