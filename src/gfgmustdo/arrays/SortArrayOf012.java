package gfgmustdo.arrays;

/**
 * Given an array A of size N containing 0s, 1s, and 2s; you need to sort the array in ascending order.
 *
 * Video ref - https://www.youtube.com/watch?v=BOt1DAvR0zI
 */
public class SortArrayOf012 {

    public static void sortArr(int[] arr) {
        int low = 0, mid = 0, high = arr.length - 1;

        while (mid <= high) {
            switch (arr[mid]) {
                case 0: {
                    swap(arr, low, mid);
                    low++;
                    mid++;
                    break;
                }
                case 1: {
                    mid++;
                    break;
                }
                case 2: {
                    swap(arr, mid, high);
                    high--;
                    break;
                }
            }
        }
    }

    private static void swap(int[] arr, int i, int j) {
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
    }

    private static void display(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + "--");
        }
        System.out.println();
    }

    public static void main(String[] args) {

        int[] arr = {1, 0, 0, 1, 2, 1, 0, 0, 0, 1, 2, 1, 2, 1, 2, 1, 0, 1, 2, 2, 2, 0};
        display(arr);
        sortArr(arr);
        display(arr);

    }


}
