package gfgmustdo.arrays;

import java.util.ArrayList;
import java.util.List;

public class StockBuyAndSell {
    class Interval {
        int buy, sell;
    }

    // This function finds the buy sell schedule for maximum profit
    void stockBuySell(int price[], int n) {
        if (price.length < 2) {
            return;
        }

        int i = 0;
        List<Interval> result = new ArrayList<>();
        while (i < (n - 1)) {
            while (i < (n - 1) && price[i + 1] <= price[i]) {
                i++;
            }

            if (i == n - 1) {
                break;
            }

            Interval interval = new Interval();
            interval.buy = i;
            i++;

            while (i < n && price[i] > price[i - 1]) {
                i++;
            }
            interval.sell = i - 1;

            result.add(interval);
        }

        result.forEach(interval -> System.out.println("Buy at :" + interval.buy + " and Sell at " + interval.sell));

    }


    public static void main(String args[]) {
        StockBuyAndSell stock = new StockBuyAndSell();

        // stock prices on consecutive days
        int price[] = {100, 180, 260, 310, 40, 535, 695};
        int n = price.length;

        // fucntion call
        stock.stockBuySell(price, n);
    }
}

