package gfgmustdo.arrays;

/***
 * Given an array A of N positive numbers. The task is to find the position where equilibrium first occurs in the array.
 * Equilibrium position in an array is a position such that the sum of elements below it is equal to the sum of elements after it.
 */
public class EquilibriumPoint {

    /**
     * Solution ref -  https://www.geeksforgeeks.org/equilibrium-index-of-an-array/
     *
     * @param arr
     * @return
     */
    static int equilibriumPoint(int[] arr) {

        int sum = 0, leftSum = 0;

        for (int i = 0; i < arr.length; i++) {
            sum += arr[i];
        }

        for (int i = 0; i < arr.length; i++) {
            sum = sum - arr[i];

            if (sum == leftSum) {
                return i;
            }

            leftSum = leftSum + arr[i];
        }

        return -1;
    }

    public static void main(String[] args) {
        System.out.println(equilibriumPoint(new int[]{-7, 1, 5, 2, -4, 3, 0}));
    }
}
