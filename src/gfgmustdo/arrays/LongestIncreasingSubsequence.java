package gfgmustdo.arrays;

import java.util.Arrays;

public class LongestIncreasingSubsequence {

    static int lisDynamic(int[] arr) {

        int[] temp = new int[arr.length];
        Arrays.fill(temp, 1);

        for (int i = 1; i < arr.length; i++) {
            for (int j = 0; j < i; j++) {
                if (arr[j] <= arr[i]) {
                    temp[i] = Math.max(temp[i], 1 + temp[j]);
                }
            }
        }

        int result = temp[0];
        for (int i = 1; i < temp.length; i++) {
            result = Math.max(result, temp[i]);
        }

        return result;

    }

    static int lis(int[] arr) {
        int maxLen = 0;

        for (int i = 0; i < arr.length; i++) {
            int len = help(arr, i);
            if (len > maxLen) {
                maxLen = len;
            }
        }
        return maxLen;
    }

    private static int help(int[] arr, int pos) {
        if (pos == 0) {
            return 1;
        }
        if (pos == arr.length) {
            return 0;
        }

        int len = 1;
        int t1 = 0;
        for (int i = 0; i < pos; i++) {
            if (arr[i] <= arr[pos]) {
                t1 = 1 + help(arr, i);
                if (t1 > len) {
                    len = t1;
                }
            }

        }

        return len;
    }

    public static void main(String[] args) {
//        System.out.println(lis(new int[]{3, 4, -1, 0, 6, 2, 3}));
        System.out.println(lis(new int[]{3, 2}));
        System.out.println(lisDynamic(new int[]{3, 2}));
    }

}
