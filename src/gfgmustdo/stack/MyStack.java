package gfgmustdo.stack;

public class MyStack {

    Node top;

    public MyStack() {
        this.top = null;
    }

    public void push(int x) {
        Node temp = new Node(x);
        if (top == null) {
            top = temp;
            return;
        }
        temp.next = top;
        top = temp;
    }

    public int pop() {
        if (top == null) {
            return -1;
        }
        Node temp = top;
        top = top.next;

        return temp.data;

    }

    public void print() {
        Node temp = top;

        while (temp != null) {
            System.out.print(temp.data + "->");
            temp = temp.next;
        }
    }

    public boolean isEmpty() {
        return top == null;
    }

    public static void main(String[] args) {
        MyStack stack = new MyStack();

        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.print();
        stack.pop();
        System.out.println();
        stack.print();
    }

}
