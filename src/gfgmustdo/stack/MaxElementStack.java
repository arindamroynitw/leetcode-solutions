package gfgmustdo.stack;

import java.util.Stack;

public class MaxElementStack {

    private Stack<Integer> stack = new Stack<>();

    private Stack<Integer> maxStack = new Stack<>();

    void push(int x) {
        stack.push(x);

        if (maxStack.isEmpty() || x > maxStack.peek()) {
            maxStack.push(x);
        }
    }

    int pop() {
        int curr = stack.pop();

        if (maxStack.peek() == curr) {
            maxStack.pop();
        }

        return curr;
    }

    int max() {
        return maxStack.peek();
    }

    public static void main(String[] args) {
        MaxElementStack stack = new MaxElementStack();

        stack.push(5);
        stack.push(4);
        stack.push(9);
        System.out.println(stack.max());
        stack.pop();
        stack.pop();
        System.out.println(stack.max());
    }
}
