package gfgmustdo.stack;

import java.util.Stack;

public class MinElementStack {

    private Stack<Integer> stack = new Stack<>();

    private Stack<Integer> minStack = new Stack<>();

    void push(Integer x) {
        stack.push(x);

        if (minStack.isEmpty() || minStack.peek() > x) {
            minStack.push(x);
        }
    }

    int pop() {
        int res = stack.pop();

        if (res == minStack.peek()) {
            minStack.pop();
        }

        return res;
    }

    int min() {
        return minStack.peek();
    }

    public static void main(String[] args) {
        MinElementStack stack = new MinElementStack();

        stack.push(5);
        stack.push(4);
        stack.push(9);
        System.out.println(stack.min());
        stack.pop();
        stack.pop();
        System.out.println(stack.min());
    }

}
