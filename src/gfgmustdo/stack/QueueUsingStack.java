package gfgmustdo.stack;

import java.util.Stack;

public class QueueUsingStack {

    Stack<Integer> s1;
    Stack<Integer> s2;

    public QueueUsingStack() {
        s1 = new Stack<>();
        s2 = new Stack<>();
    }

    public void enQueue(int x) {
        if (s1.isEmpty()) {
            s1.push(x);
            return;
        }

        while (!s1.isEmpty()) {
            int temp = s1.pop();
            s2.push(temp);
        }

        s1.push(x);

        while (!s2.isEmpty()) {
            int temp = s2.pop();
            s1.push(temp);
        }

    }

    public int deQueue() {
        if(s1.isEmpty()) {
            return Integer.MAX_VALUE;
        }
        return s1.pop();
    }

    public static void main(String[] args) {
        QueueUsingStack queue = new QueueUsingStack();
        queue.enQueue(1);
        queue.enQueue(2);
        queue.enQueue(3);
        System.out.println(queue.deQueue());
        System.out.println(queue.deQueue());
        queue.enQueue(4);
        System.out.println(queue.deQueue());
    }
}
