package gfgmustdo.stack;

import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

/**
 * Given an array A of size N having distinct elements,
 * the task is to find the next greater element for each
 * element of the array in order of their appearance in the array. If no such element exists, output -1
 */
public class NextGreaterElement {


    static Map<Integer, Integer> nextGreaterElement(int[] arr) {

        Map<Integer, Integer> result = new HashMap<>();

        Stack<Integer> stack = new Stack<>();

        for (int i = 0; i < arr.length; i++) {

            if (stack.isEmpty() || stack.peek() > arr[i]) {
                stack.push(arr[i]);
            } else {
                while (!stack.isEmpty() && stack.peek() < arr[i]) {
                    int next = stack.pop();
                    result.put(next, arr[i]);
                }
                stack.push(arr[i]);
            }
        }

        while (!stack.isEmpty()) {
            int next = stack.pop();
            result.put(next, -1);
        }

        return result;
    }

    public static void main(String[] args) {
        int[] arr = {13, 7, 6, 12};

        nextGreaterElement(arr).forEach((k, v) -> System.out.println(k + " -> " + v));
    }

}
