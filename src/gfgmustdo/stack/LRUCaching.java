package gfgmustdo.stack;

import java.util.HashMap;

class CacheNode {
    int key;
    int value;
    CacheNode prev;
    CacheNode next;

    public CacheNode(int key, int value) {
        this.key = key;
        this.value = value;
    }
}

public class LRUCaching {
    int capacity;
    HashMap<Integer, CacheNode> cacheMap = new HashMap<>();
    CacheNode head = null;
    CacheNode tail = null;

    public LRUCaching(int capacity) {
        this.capacity = capacity;
    }

    public int get(int key) {
        if (cacheMap.containsKey(key)) {
            CacheNode node = cacheMap.get(key);
            remove(node);
            setHead(node);
            return node.value;
        }

        return -1;
    }

    public void put(int key, int value) {
        CacheNode node = new CacheNode(key, value);

        if (cacheMap.containsKey(key)) {
            CacheNode old = cacheMap.get(key);
            remove(old);
            setHead(old);
        } else {
            if (cacheMap.size() == capacity) {
                remove(tail);
                cacheMap.remove(tail.key);
                setHead(node);
            } else {
                setHead(node);
            }
            cacheMap.put(key, node);

        }
    }

    private void setHead(CacheNode node) {

        node.next = head;
        node.prev = null;

        if (head != null) {
            head.prev = node;
        }

        head = node;

        if (tail == null) {
            tail = head;
        }
    }

    private void remove(CacheNode node) {

        if (node.prev != null) {
            node.prev.next = node.next;
        } else {
            head = node.next;
        }

        if (node.next != null) {
            node.next.prev = node.prev;
        } else {
            tail = node.prev;
        }

    }
}
