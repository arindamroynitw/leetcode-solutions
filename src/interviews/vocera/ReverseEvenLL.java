package interviews.vocera;

/**
 * You are given a linked list that contains  integers. You have performed the following reverse operation on the list:
 * <p>
 * Select all the subparts of the list that contain only even integers. For example, if the list is , then the selected subparts will be , .
 * Reverse the selected subpart such as  and .
 * Now, you are required to retrieve the original list.
 */
class Node {
    int data;
    Node next;

    public Node(int data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return String.valueOf(data);
    }
}

public class ReverseEvenLL {


    static Node reverse(Node head) {

        Node prev = null, curr = head, next = null;

        while (curr != null) {
            next = curr.next;
            curr.next = prev;
            prev = curr;
            curr = next;
        }

        return prev;
    }


    static Node reverseItoJ(Node head, Node previous, Node start, Node end) {
        if (start == end) {
            return head;
        }

        Node nextEnd = end.next;
        end.next = null;
        Node newStart = reverse(start);

        start.next = nextEnd;
        if (previous != null)
            previous.next = end;

        if(start==head) {
            return newStart;
        }
        return head;

    }


    public static void main(String[] args) {
        Node head = new Node(1);
        head.next = new Node(2);
        head.next.next = new Node(3);
        head.next.next.next = new Node(4);
        head.next.next.next.next = new Node(5);

        head = reverseItoJ(head, head, head.next, head.next.next.next.next);

        System.out.println(head);
    }

}
