package interviews;

// Class name must be "SG"
// Libraries included:
// json simple, guava, apache commons lang3, junit, jmock
//Access
// Queue
// struct Message {
//     int id;
//     string content;
// }

// q = new Queue();
// q.enqueue(Message m);
// q.dequeue() -> Message;
// q.delete(int id);
import java.util.HashMap;
import java.util.Objects;

class Message {
    int id;
    String content;

    public Message(int id, String content) {
        this.id = id;
        this.content = content;
    }
}

class Node {
    Message message;
    Node next;

    public Node(Message message) {
        this.message = message;
    }
}

class MyQueue {
    Node front;
    Node rear;
    HashMap<Integer, Node> kvMap;

    public MyQueue() {
        front = null;
        rear = null;

        kvMap = new HashMap<>();
    }

    public void enQueue(Message m) {
        Node temp = new Node(m);

        if(front == null) {
            front = temp;
            rear = front;
        } else {

            rear.next = temp;
            rear = temp;
        }

        kvMap.put(temp.message.id, temp);
    }

    public Message deQueue() {
        Node temp = front;
        if(temp == null) {
            return null;
        }

        front = front.next;

        if(front == null) {
            rear = null;
        }

        kvMap.remove(temp.message.id);
        return temp.message;
    }

    public Message delete(int id) {
        Node toBeDeleted = searchElement(id);
        if(Objects.isNull(toBeDeleted)) {
            return null;
        }
        return deleteNode(toBeDeleted);
    }

    private Node searchElement(int id) {
        if(kvMap.containsKey(id)) {
            return kvMap.get(id);
        } else {
            return null;
        }
    }

    private Message deleteNode(Node node) {
        Node nextNode = node.next;

        Message temp = node.message;

        node.message = nextNode.message;

        node.next = nextNode.next;

        return temp;
    }

}

public class ShopeeCode {
    public static void main(String[] args) {
        MyQueue queue = new MyQueue();

        queue.enQueue(new Message(1, "Hi"));
        queue.enQueue(new Message(2, "Hello"));
        System.out.println(queue.deQueue().content);
        System.out.println(queue.deQueue().content);
        queue.enQueue(new Message(3, "Code"));
        System.out.println(queue.deQueue().content);

        queue.enQueue(new Message(1, "Hi"));
        queue.enQueue(new Message(2, "Hello"));
        queue.enQueue(new Message(3, "Code"));

        System.out.println(queue.delete(2).content);

        System.out.println(queue.deQueue().content);
        System.out.println(queue.deQueue().content);


    }
}


/*
Old Content below(Python 2):
*/




