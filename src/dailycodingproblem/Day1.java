package dailycodingproblem;

import java.util.HashMap;
import java.util.Map;

/**
 * Given a list of numbers and a number k, return whether any two numbers from the list add up to k.
 * <p>
 * For example, given [10, 15, 3, 7] and k of 17, return true since 10 + 7 is 17.
 * <p>
 * Bonus: Can you do this in one pass?
 */
public class Day1 {

    public static boolean isPossible(int[] list, int target) {

        Map<Integer, Integer> myMap = new HashMap<>();

        for (int i = 0; i < list.length; i++) {
            if (myMap.containsKey(target - list[i])) {
                return true;
            } else {
                myMap.put(list[i], i);
            }
        }
        return false;

    }

    public static void main(String[] args) {
        System.out.println(isPossible(new int[]{10, 15, 3, 7}, 25));
    }

}
