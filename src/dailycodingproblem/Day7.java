package dailycodingproblem;

/**
 * Given the mapping a = 1, b = 2, ... z = 26, and an encoded message,
 * count the number of ways it can be decoded.
 * <p>
 * CSDOJO Video Link - https://youtu.be/qli-JCrSwuk
 */
public class Day7 {

    public static int numwaysDynamic(String message) {

        int[] counts = new int[message.length() + 1];

        for (int i = 0; i < counts.length; i++) {
            counts[i] = -1;
        }

        return helperDynamic(message, message.length(), counts);

    }

    private static int helperDynamic(String message, int length, int[] counts) {

        if (length == 0) {
            return 1;
        }

        int startIndex = message.length() - length;

        if (message.charAt(startIndex) == '0') {
            return 0;
        }

        if (counts[length] != -1) {
            return counts[length];
        }

        counts[length] = helperDynamic(message, length - 1, counts);
        if (length >= 2 && Integer.valueOf(message.substring(startIndex, startIndex + 2)) <= 26) {
            counts[length] += helperDynamic(message, length - 2, counts);
        }

        return counts[length];

    }

    public static int numways(String message) {
        return helper(message, message.length());
    }

    private static int helper(String message, int length) {

        if (length == 0) {
            return 1;
        }

        int startIndex = message.length() - length;

        if (message.charAt(startIndex) == '0') {
            return 0;
        }

        int result = helper(message, length - 1);

        if (length >= 2 && Integer.valueOf(message.substring(startIndex, startIndex + 2)) <= 26) {
            result += helper(message, length - 2);
        }

        return result;
    }

    public static void main(String[] args) {
        Long before = System.currentTimeMillis();
        System.out.println(numwaysDynamic("111111111111111111111111"));
        Long after = System.currentTimeMillis();
        System.out.println("Time : " + (after - before));

        before = System.currentTimeMillis();
        System.out.println(numwaysDynamic("111111111111111111111111"));
        after = System.currentTimeMillis();
        System.out.println("Time :" + (after - before));
    }
}
