package dailycodingproblem;

/**
 * Given a list of integers, write a function that returns the largest sum of non-adjacent numbers. Numbers can be 0 or negative.
 * <p>
 * For example, [2, 4, 6, 2, 5] should return 13, since we pick 2, 6, and 5. [5, 1, 1, 5] should return 10, since we pick 5 and 5.
 * <p>
 * Follow-up: Can you do this in O(N) time and constant space?
 * <p>
 * Video ref - https://youtu.be/UtGtF6nc35g
 */
public class Day9 {

    public static int getMaxSumNonAdjacent(int[] arr) {

        int incl = arr[0], excl = 0;

        for (int i = 1; i < arr.length; i++) {
            int temp = incl;
            incl = Math.max(excl + arr[i], incl);
            excl = temp;
        }

        return Math.max(incl, excl);
    }

    public static void main(String[] args) {
        System.out.println(getMaxSumNonAdjacent(new int[]{5, 5, 10, 9, 4, 5}));
    }

}
