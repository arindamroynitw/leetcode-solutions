package dailycodingproblem;

import java.util.Arrays;
import java.util.List;

public class Test2 {

    public static final String A = "A";
    public static final String B = "B";
    public static final String C = "C";
    public static final String D = "D";
    public static final String G = "G";
    public static final String E = "E";
    public static final String F = "F";
    public static final String H = "H";
    public static final String J = "J";
    public static final String K = "K";
    public static final String SEAT_SPLIT_REGEX = " ";

    public int getAvailableSeats(int N, String S) {
        String[] takenSeats = S.split(SEAT_SPLIT_REGEX);
        List<String> reservedSeats = Arrays.asList(takenSeats); //Just to use Java library contains method

        int possibleThreeSeaters = 0;

        for (int row = 1; row <= N; row++) {

            if (checkSeatSet1(reservedSeats, row)) {
                possibleThreeSeaters++;
            }
            if (checkSeatSet2(reservedSeats, row)) {
                possibleThreeSeaters++;
            }
            if (checkSeatSet3(reservedSeats, row)) {
                possibleThreeSeaters++;
            }
        }
        return possibleThreeSeaters;
    }

    public boolean checkSeatSet1(List<String> reservedSeats, int rowNum) {
        //All three seats need to be empty to be empty to seat in this row
        return (!reservedSeats.contains(rowNum + A) && !reservedSeats.contains(rowNum + B) && !reservedSeats.contains(rowNum + C));
    }

    public boolean checkSeatSet2(List<String> reservedSeats, int rowNum) {
        //Either first three are empty, or last three are empty

        return ((!reservedSeats.contains(rowNum + D) && !reservedSeats.contains(rowNum + E) && !reservedSeats.contains(rowNum + F)) ||
                (!reservedSeats.contains(rowNum + E) && !reservedSeats.contains(rowNum + F) && !reservedSeats.contains(rowNum + G)));
    }

    public boolean checkSeatSet3(List<String> reservedSeats, int rowNum) {
        //All three seats need to be empty to be empty to seat in this row
        return (!reservedSeats.contains(rowNum + H) && !reservedSeats.contains(rowNum + J) && !reservedSeats.contains(rowNum + K));

    }


    public static void main(String[] args) {

        Test2 t = new Test2();

        System.out.println(t.getAvailableSeats(10, "1A 1E 1K 2G 3B 3H 3F 4D"));

    }
}
