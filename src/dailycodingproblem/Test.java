package dailycodingproblem;

public class Test {

    String HOURS = "h";
    String MINUTES = "m";
    String SECONDS = "s";


    public String solution(int T) {

        if (T < 60) {
            return T + SECONDS;
        }

        int minutes = T / 60;
        T = T - (minutes * 60);

        if (minutes < 60) {
            return minutes + MINUTES + T + SECONDS;
        }

        int hours = minutes / 60;

        minutes = minutes - (hours * 60);

        return hours + HOURS + minutes + MINUTES + T + SECONDS;

    }

    public static void main(String[] args) {

        Test test = new Test();

        System.out.println(test.solution(7261));
    }
}
