package dailycodingproblem;

/**
 * Given an array of integers, return a new array such that each element at index i of the new array is the product of all the numbers in the original array except the one at i.
 * <p>
 * For example, if our input was [1, 2, 3, 4, 5], the expected output would be [120, 60, 40, 30, 24]. If our input was [3, 2, 1], the expected output would be [2, 3, 6].
 * <p>
 * Follow-up: what if you can't use division?
 */
public class Day2 {

    public static int[] getResultWithDivision(int[] list) {

        int total = 1;

        for (int i : list) {
            total *= i;
        }

        int[] result = new int[list.length];

        for (int i = 0; i < list.length; i++) {
            result[i] = total / list[i];
        }

        return result;
    }


    public static int[] getResultWithoutDivision(int[] list) {

        int[] leftResult = new int[list.length];
        int[] rightResult = new int[list.length];

        leftResult[0] = 1;
        for (int i = 1; i < list.length; i++) {
            leftResult[i] = leftResult[i - 1] * list[i - 1];
        }

        rightResult[list.length - 1] = 1;
        for (int i = list.length - 2; i >= 0; i--) {
            rightResult[i] = rightResult[i + 1] * list[i + 1];
        }

        for (int i = 0; i < list.length; i++) {
            leftResult[i] = leftResult[i] * rightResult[i];
        }
        return leftResult;
    }

    public static void main(String[] args) {
        int[] result = getResultWithDivision(new int[]{1, 2, 3, 4, 5});
        int[] result2 = getResultWithoutDivision(new int[]{1, 2, 3, 4, 5});

        for (int i = 0; i < result.length; i++) {
            System.out.print(result[i] + " ");
        }
        System.out.println("");

        for (int i = 0; i < result2.length; i++) {
            System.out.print(result2[i] + " ");
        }
    }
}
