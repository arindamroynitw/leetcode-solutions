package dailycodingproblem;

public class SpaceExtract {

    static String modifyString(String s) {

        char[] arr = s.toCharArray();

        String result = "";

        int counter = 1;

        for (int i = 0; i < arr.length; ) {
            if (i != arr.length - 1 && arr[i] == arr[i + 1]) {
                counter++;
                i++;
            } else {
                result = result + arr[i];
                if (counter > 1) {
                    result = result + counter;
                    counter = 1;
                }
                i++;
            }
        }

        return result;

    }

    public static void main(String[] args) {

        System.out.println(modifyString("aabbcccdddefgaaa"));

    }

}
