package easy;

import java.util.Stack;

/**
 * Given a string containing just the characters '(', ')', '{', '}', '[' and ']', determine if the input string is valid.
 * <p>
 * An input string is valid if:
 * <p>
 * Open brackets must be closed by the same type of brackets.
 * Open brackets must be closed in the correct order.
 * Note that an empty string is also considered valid.
 * <p>
 * Example 1:
 * <p>
 * Input: "()"
 * Output: true
 * Example 2:
 * <p>
 * Input: "()[]{}"
 * Output: true
 * Example 3:
 * <p>
 * Input: "(]"
 * Output: false
 * Example 4:
 * <p>
 * Input: "([)]"
 * Output: false
 * Example 5:
 * <p>
 * Input: "{[]}"
 * Output: true
 */
public class Prob20 {

    public boolean isValid(String s) {

        if (s == null || s.length() == 0) {
            return true;
        }

        Stack<Character> content = new Stack<>();
        char[] arr = s.toCharArray();

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == '{' || arr[i] == '[' || arr[i] == '(') {
                content.push(arr[i]);
            }

            if (arr[i] == '}' || arr[i] == ')' || arr[i] == ']') {

                if (content.isEmpty() || content.peek() == null) {
                    return false;
                }

                if (arr[i] == '}') {
                    char x = content.pop();
                    if (x != '{') {
                        return false;
                    }
                }

                if (arr[i] == ')') {
                    char x = content.pop();
                    if (x != '(') {
                        return false;
                    }
                }
                if (arr[i] == ']') {
                    char x = content.pop();
                    if (x != '[') {
                        return false;
                    }
                }
            }
        }

        if (!content.isEmpty()) {
            return false;
        }

        return true;
    }

    public static void main(String[] args) {
        Prob20 prob20 = new Prob20();

        System.out.println(prob20.isValid("(){{}"));
    }

}
