package easy;

public class Prob28 {

    public int strStr(String haystack, String needle) {

        if(needle.length() == 0) {
            return 0;
        }

        if(haystack.indexOf(needle)!=-1) {
            return haystack.indexOf(needle);
        }

        return -1;
    }

    public static void main(String[] args) {
        Prob28 prob28 = new Prob28();

        System.out.println(prob28.strStr("hello", "lll"));
    }

}
