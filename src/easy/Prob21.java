package easy;

import medium.ListNode;

public class Prob21 {

    public ListNode mergeTwoLists(ListNode l1, ListNode l2) {

        ListNode dummyHead = new ListNode(Integer.MAX_VALUE);
        ListNode ptr = dummyHead;

        while (l1 != null && l2 != null) {
            if (l1.val < l2.val) {
                ptr.next = new ListNode(l1.val);
                ptr = ptr.next;
                l1 = l1.next;
            } else {
                ptr.next = new ListNode(l2.val);
                ptr = ptr.next;
                l2 = l2.next;
            }
        }

        if (l1 != null) {
            while (l1 != null) {
                ptr.next = new ListNode(l1.val);
                ptr = ptr.next;
                l1 = l1.next;
            }
        }

        if (l2 != null) {
            while (l2 != null) {
                ptr.next = new ListNode(l2.val);
                ptr = ptr.next;
                l2 = l2.next;
            }
        }

        return dummyHead.next;

    }

    public static void main(String[] args) {

        ListNode l1 = new ListNode(-9);
        l1.next = new ListNode(3);
//        l1.next.next = new ListNode(5);

        ListNode l2 = new ListNode(5);
        l2.next = new ListNode(7);

        Prob21 prob21 = new Prob21();

        prob21.display(prob21.mergeTwoLists(l1, l2));

    }

    void display(ListNode head) {
        while (head != null) {
            System.out.print(head.val + "->");
            head = head.next;
        }
        System.out.println();
    }


}
