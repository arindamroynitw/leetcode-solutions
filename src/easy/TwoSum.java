package easy;

import java.util.HashMap;
import java.util.Map;

/**
 * Question - Given an array of integers, return indices of the two numbers such that they add up to a specific target.
 *
 * You may assume that each input would have exactly one solution, and you may not use the same element twice.
 */
public class TwoSum {

    public static void main(String[] args) {
        Solution solution = new Solution();

        int[] answer = solution.twoSum(new int[]{10,7,9,8,11}, 21);

        System.out.println(answer[0] + answer[1]);
    }
}

/**
 * Solution -
 * 1. Create a map of integer.
 * 2. For every element of nums[], check if map has entry for (target - nums[i]).
 * 3. If map has (target - nums[i]), it means num[i] and (target - nums[i]) are both in the array, so return their indices.
 * 4. If map does not have (target - nums[i]), make entry for num[i]->i to store current index.
 */
class Solution {
    public int[] twoSum(int[] nums, int target) {

        Map<Integer, Integer> map = new HashMap<>();

        for (int i = 0; i < nums.length; i++) {
            int diff = target - nums[i];
            if (map.containsKey(diff)) {
                return new int[]{map.get(diff), i};
            }
            map.put(nums[i], i);
        }
        return null;
    }
}
