package easy;

public class Prob122 {

    public int maxProfit(int[] prices) {
        if (prices.length < 2) {
            return 0;
        }

        int n = prices.length;
        int i = 0;
        int localMaxima = 0;
        int res = 0;
        while (i < (n - 1)) {
            while (i < (n - 1) && prices[i + 1] <= prices[i]) {
                i++;
            }

            if (i == n - 1) {
                break;
            }

            localMaxima = i++;

            while (i < n && prices[i] > prices[i - 1]) {
                i++;
            }

            res = res + prices[i-1] - prices[localMaxima];

        }


        return res;


    }
}
