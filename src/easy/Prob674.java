package easy;

public class Prob674 {

    public static int findLengthOfLCIS(int[] nums) {

        if (nums.length < 2) {
            return nums.length;
        }

        int count = 0;

        int result = 0;
        for (int i = 0; i < nums.length - 1; i++) {
            if (nums[i] < nums[i + 1]) {
                count += 1;
                result = Math.max(count, result);
            } else {
                count = 0;
            }
        }

        return result + 1;
    }

    public static void main(String[] args) {
        System.out.println(findLengthOfLCIS(new int[]{1, 3, 5, 4, 7}));
    }
}
