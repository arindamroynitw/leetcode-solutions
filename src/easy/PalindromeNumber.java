package easy;

public class PalindromeNumber {

    /**
     * Solution -
     * <p>
     * 1. Find highest place value of the number in variable metric.
     * <p>
     * 2. Run loop on number. Everytime check first and last digit, and get rid of them if they match. Then reduce metric
     * by 10 and repeat.
     */
    public boolean isPalindrome(int x) {

        if (x < 0) return false;

        int metric = 1;

        while (x / metric >= 10) {
            metric *= 10;
        }

        while (x > 0) {
            int left = x / metric;
            int right = x % 10;
            if (left != right) return false;

            x = (x % metric) / 10;

            metric = metric / 100;

        }

        return true;

    }

    static boolean isPalindrome(String word) {
        int length = word.length();
        if (length == 0)
            return true;
        word = Character.toLowerCase(word.charAt(0)) + word.substring(1);
        return helper(word, 0, length - 1);
    }


    static boolean helper(String word,
                          int start, int end) {
        if (start == end)
            return true;

        if ((word.charAt(start)) != (word.charAt(end)))
            return false;

        if (start < end + 1)
            return helper(word, start + 1, end - 1);

        return true;
    }


    public static void main(String[] args) {

        PalindromeNumber p = new PalindromeNumber();
        System.out.println(p.isPalindrome(12321));
        System.out.println(p.isPalindrome(1221));
        ;
    }

}
