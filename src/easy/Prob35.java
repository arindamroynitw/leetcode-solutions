package easy;

public class Prob35 {

    public int searchInsert(int[] nums, int target) {

        if (nums == null)
            return -1;
        if (target > nums[nums.length - 1]) {
            return nums.length;
        }

        int i = 0;
        int j = nums.length;

        while (i < j) {
            int m = (i + j) / 2;
            if (target > nums[m]) {
                i = m + 1;
            } else {
                j = m;
            }
        }

        return j;
    }


    public static void main(String[] args) {
        Prob35 prob35 = new Prob35();
        System.out.println(prob35.searchInsert(new int[]{1, 3, 5, 6}, 2));
    }

}
