package easy;

/**
 * Question - Given a 32-bit signed integer, reverse digits of an integer.
 */
public class ReverseInteger {

    public static void main(String[] args) {
        RevIntSolution solution = new RevIntSolution();

        System.out.println(solution.reverse(1534236469));
    }
}

/**
 * Solution -
 * 1. Negative check and flip.
 * 2. Use mod 10 -> by 10 to reverse the number, while checking for overflow at each step.
 * 3. Overflow algo - if reversing the operation doesn't give original value, there is an overflow.
 */
class RevIntSolution {

    public int reverse(int x) {

        boolean neg = x < 0;

        if (neg)
            x = 0 - x;

        int result = 0;

        while (x != 0) {
            int current = result * 10 + x % 10;

            //Overflow check
            if ((current - x % 10) / 10 != result) return 0;

            else result = current;

            x = x / 10;
        }


        return neg ? 0 - result : result;
    }
}
