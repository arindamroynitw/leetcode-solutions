package easy;

public class Prob27 {

    public int removeElement(int[] nums, int val) {

        if (nums == null || nums.length == 0) {
            return nums.length;
        }

        if (nums.length == 1) {
            if (nums[0] == val) {
                return 0;
            } else {
                return 1;
            }
        }

        int i = 0;

        int j = nums.length - 1;

        while (i < j) {
            if (nums[i] != val) {
                i++;
            } else {
                if (nums[j] != val) {
                    int temp = nums[i];
                    nums[i] = nums[j];
                    nums[j] = temp;
                    i++;
                    j--;
                } else {
                    j--;
                }
            }
        }

        return (nums[i] != val) ? i + 1 : i;
    }

    public static void main(String[] args) {
        Prob27 prob27 = new Prob27();

        System.out.println(prob27.removeElement(new int[]{2, 2}, 2));
    }

}
